﻿class Codigoendesuso
{

    #region Primera versión del código usado en menús de combate
    /*
    
    private IEnumerator AtaqueBasico(Character personaje)
    {
        siguienteAccionTurno = false;
        panelObjetivos.SetVisible(true);
        while (!siguienteAccionTurno) //Espera a que se seleccione un objetivo
        {
            if (VolverAlMenuAnterior()) //Cancela el ataque al pulsar el botón de correr/cancelar
            {
                //El menú anterior es el principal, asi que sale de la función
                CambioEstado((int)EstadoTurno.EN_ESPERA);
                panelObjetivos.SetVisible(false);
                yield return null;
                yield break;
            }
            yield return null;
        }
        panelObjetivos.SetVisible(false);

        AttackInfo<Character> atInfo = new AttackInfo<Character>()
        {
            attacker = personaje,
            skill = NormalAttack.Singleton,
            target = elementoMenuSeleccionado
        };

        TriggerAnimation(personaje, triggerAttack);

        RecibirAtaqueGeneral(enemigos, atInfo);

        CambioEstado((int)EstadoTurno.TURNO_TERMINADO);
    }


    private IEnumerator MenuSkillsOLD(Character personaje)
    {
        elementoMenuSeleccionado = 0;

        List<Skill> skills = personaje.skills;
        List<string> skillNames = new List<string>();
        List<string> skillCosts = new List<string>();
        foreach (var skill in skills)
        {
            skillNames.Add(skill.nombre);
            skillCosts.Add(skill.consumo + (skill.type == TypeOfSkill.SPELL ? " MP" : " EP"));
        }


        Skill ataqueElegido = null;

        panelAuxiliar.gameObject.SetActive(true);
        panelAuxiliar.SetData(skillNames, skillCosts);
        bool puedeUsarse = false;
        do
        {
            siguienteAccionTurno = false;
            while (!siguienteAccionTurno) //Espera a que se seleccione un objetivo
            {
                if (VolverAlMenuAnterior())
                {
                    //Volvemos al estado base del turno (EN_ESPERA o MENU_PRINCIPAL)
                    panelAuxiliar.gameObject.SetActive(false);
                    CambioEstado((int)EstadoTurno.EN_ESPERA);
                    yield return null;
                    yield break; //Cancela la selección de habilidad/hechizo y vuelve al menú principal
                }
                yield return null;
            }

            ataqueElegido = skills[elementoMenuSeleccionado];

            if (ataqueElegido != null)
            {
                puedeUsarse = SkillLanzable(personaje, ataqueElegido);
            }

        } while (!puedeUsarse);

        if (ataqueElegido != null)
        {

            List<Stats> posiblesObjetivos = GetTargets(ataqueElegido.area, ataqueElegido.objetivo);
            List<string> listaNombres = GetNames(posiblesObjetivos);

            switch (ataqueElegido.area)
            {
                case Area.SINGLE:
                    {
                        siguienteAccionTurno = false;
                        panelObjetivos.SetData(listaNombres);
                        panelObjetivos.SetVisible(true);
                        while (!siguienteAccionTurno) //Espera a que se seleccione un objetivo
                        {
                            //Si pulsamos el botón de cancelar/correr volvemos al menú anterior
                            //TODO debe haber una mejor manera de hacer esto, tal y como esta ahora mismo hay que copiar el mismo
                            //código en varias partes con ligeros cambios para acomodar qué menú específico se está usando
                            if (VolverAlMenuAnterior())
                            {
                                panelObjetivos.SetVisible(false);
                                yield return null;
                                yield break; //Cancela el ataque y vuelve a abrir el menú de habilidades
                            }
                            else
                            {
                                //Esto podria estar fuera del else si no fuera porque buscamos una variable que solo esta activa por un frame
                                yield return null;
                            }
                        }
                        panelAuxiliar.SetVisible(false);


                        AttackInfo<Character> atInfo = new AttackInfo<Character>()
                        {
                            attacker = personaje,
                            skill = ataqueElegido,
                            target = elementoMenuSeleccionado
                        };

                        TriggerAnimation(personaje, triggerSpecial);

                        switch (ataqueElegido.objetivo)
                        {
                            case Target.ENEMIES:
                                RecibirAtaqueGeneral(enemigos, atInfo);
                                break;
                            case Target.ALLIES:
                                RecibirAtaqueGeneral(aliados, atInfo);
                                break;
                            case Target.ALL: //TODO Ataca a todos o puede atacar a todos?
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Area.ALL:
                    {
                        panelAuxiliar.gameObject.SetActive(false);
                        AttackInfo<Character> atInfo = new AttackInfo<Character>()
                        {
                            attacker = personaje,
                            skill = ataqueElegido,
                            target = 0
                        };

                        TriggerAnimation(personaje, triggerSpecial);

                        switch (ataqueElegido.objetivo)
                        {
                            case Target.ENEMIES:
                                RecibirAtaqueGeneral(enemigos, atInfo);
                                break;
                            case Target.ALLIES:
                                RecibirAtaqueGeneral(aliados, atInfo);
                                break;
                            case Target.ALL: //Ambos bandos reciben el ataque
                                RecibirAtaqueGeneral(enemigos, atInfo);
                                RecibirAtaqueGeneral(aliados, atInfo);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Area.RANDOM: //TODO Decidir cómo atacar a enemigos aleatoriamente
                    break;
                default:
                    break;
            }

            switch (ataqueElegido.type)
            {
                case TypeOfSkill.ABILITY:
                    personaje.energy -= ataqueElegido.consumo;
                    break;
                case TypeOfSkill.SPELL:
                    personaje.mp -= ataqueElegido.consumo;
                    break;
                default:
                    break;
            }

        }

        CambioEstado((int)EstadoTurno.TURNO_TERMINADO);
    }

    private IEnumerator MenuItemsOLD(Character personaje)
    {
        elementoMenuSeleccionado = 0;

        var datos = DatosEntreEscenasController.Singleton;

        List<Consumable> items = datos.currentSave.inventario.GetItemsOfType<Consumable>();
        List<string> itemNames = new List<string>();
        List<string> itemAmounts = new List<string>();
        foreach (var item in items)
        {
            itemNames.Add(item.nombre);
            itemAmounts.Add(item.cantidad.ToString());
        }

        panelAuxiliar.SetData(itemNames, itemAmounts);

        Consumable itemElegido = null;

        panelAuxiliar.SetVisible(true);
        siguienteAccionTurno = false;
        while (!siguienteAccionTurno) //Espera a que se seleccione un objeto
        {
            //Si pulsamos el botón de cancelar/correr volvemos al menú anterior
            //TODO debe haber una mejor manera de hacer esto, tal y como esta ahora mismo hay que copiar el mismo
            //código en varias partes con ligeros cambios para acomodar qué menú específico se está usando
            if (VolverAlMenuAnterior())
            {
                panelAuxiliar.gameObject.SetActive(false);
                CambioEstado((int)EstadoTurno.EN_ESPERA);
                yield return null;
                yield break; //Cancela la selección de objeto y vuelve al menú de acciones
            }
            else
            {
                //Esto podria estar fuera del else si no fuera porque buscamos una variable que solo esta activa por un frame
                yield return null;
            }
        }

        itemElegido = items[elementoMenuSeleccionado];
        elementoMenuSeleccionado = 0;

        panelObjetivos.SetVisible(true);
        siguienteAccionTurno = false;
        while (!siguienteAccionTurno) //Espera a que se seleccione un objetivo
        {
            //Si pulsamos el botón de cancelar/correr volvemos al menú anterior
            //TODO debe haber una mejor manera de hacer esto, tal y como esta ahora mismo hay que copiar el mismo
            //código en varias partes con ligeros cambios para acomodar qué menú específico se está usando
            if (VolverAlMenuAnterior())
            {
                panelObjetivos.SetVisible(false);
                yield return null;
                yield break; //Cancela la selección de objetivo y vuelve a la selección de item
            }
            else
            {
                //Esto podria estar fuera del else si no fuera porque buscamos una variable que solo esta activa por un frame
                yield return null;
            }

        }

        panelObjetivos.SetVisible(false);
        panelAuxiliar.gameObject.SetActive(false);


        if (itemElegido != null)
        {
            //TODO llenar lista de objetivos dinámicamente
            List<Stats> posiblesObjetivos = GetTargets(itemElegido.area, itemElegido.objetivo); //Por ahora solo se puede usar en aliados o enemigos, no muestra nombre en la lista

            switch (itemElegido.area)
            {
                case Area.SINGLE:
                    {
                        //Código de selección de objetivo
                        //Usa el objeto en sólo un objetivo
                    }
                    break;
                case Area.ALL:
                    itemElegido.Consumir(posiblesObjetivos); //Usa el objeto en todos
                    break;
                case Area.RANDOM:
                    //TODO Cómo decidimos aleatoriamente quien recibe el efecto?
                    break;
                default:
                    break;
            }

        }

        yield return null;
        CambioEstado((int)EstadoTurno.TURNO_TERMINADO);
    }
    */
    #endregion


    #region Funciones de ataque obsoletas (Combate)
    /*
public void AtaqueRandomOLD()
{
    if (UnityEngine.Random.Range(0, 2) > 0)
    {
        FormacionEquipo<Character> formacion = FormacionAliada;

        var FormacionPos = (FormacionPos)UnityEngine.Random.Range(0, 2);

        int y;

        Character temp;

        do
        {
            y = UnityEngine.Random.Range(0, 5);
            temp = formacion.formacion[(int)FormacionPos][y];
        } while (temp == null);

        AtacarUno(formacion, FormacionPos, y, 100f);

    }
    else
    {
        FormacionEquipo<Monster> formacion = FormacionEnemiga;

        var FormacionPos = (FormacionPos)UnityEngine.Random.Range(0, 2);

        int y;

        Monster temp;

        do
        {
            y = UnityEngine.Random.Range(0, 5);
            temp = formacion.formacion[(int)FormacionPos][y];
        } while (temp == null);

        AtacarUno(formacion, FormacionPos, y, 100f);
    }
}


private void AtacarUno<T>(FormacionEquipo<T> equipoObjetivo, FormacionPos objetivoX, int objetivoY, float fuerzaAtaque) where T : Stats
{
    if (objetivoX == FormacionPos.RETAGUARDIA)
    {
        RecibirAtaqueRetaguardia(equipoObjetivo, objetivoY, fuerzaAtaque);
    }
    else if (objetivoX == FormacionPos.VANGUARDIA)
    {
        RecibirAtaqueVanguardia(equipoObjetivo.formacion[(int)objetivoX][objetivoY], fuerzaAtaque);
    }
}

private void RecibirAtaqueRetaguardia<T>(FormacionEquipo<T> equipoObjetivo, int pos, float fuerzaAtaque) where T : Stats
{
    //Recibe el ataque como la vanguardia, pero parte del daño es redirigido a quienquiera que tenga delante dependiendo de la formación

    T[] vanguardia = equipoObjetivo.formacion[0];

    T objetivo = equipoObjetivo.formacion[1][pos]; //El objetivo tiene que estar en la retaguardia

    if (objetivo == null)
    {
        Debug.LogError("Se ha puesto como objetivo un espacio vacío");
        return;
    }


    //int countVanguardia = vanguardia.Where(x => x != null).Count();

    float reduccionAtaque = 0; //Porcentaje a reducir para quien ha recibido el daño en la retaguardia

    float ataqueDividido;

    List<T> defensores = new List<T>();

    //Bucle mientras i este entre las 5 posiciones delante del objetivo e i sea menor al límite del array
    for (int i = pos - 2; i <= pos + 2 && i < vanguardia.Length; i++)
    {
        if (i < 0) continue;

        T temp = equipoObjetivo.formacion[0][i];
        if (temp != null)
        {
            reduccionAtaque += 0.25f;
            //Reduccion de daño limitada al 85%, por si algún dia ponemos grupos de enemigos de mas de 5, o con formaciones que no podemos hacer
            if (reduccionAtaque > 0.85f) reduccionAtaque = 0.85f;

            defensores.Add(temp);
        }
    }

    ataqueDividido = fuerzaAtaque * (reduccionAtaque / defensores.Count);

    //Los que estén delante de quien recibe el ataque absorben su parte del daño base
    foreach (T personaje in defensores)
    {
        RecibirAtaqueVanguardia(personaje, ataqueDividido);
    }

    RecibirAtaqueVanguardia(objetivo, (1 - reduccionAtaque) * fuerzaAtaque); //El objetivo recibe el daño mitigado por sus compañeros de la vanguardia

}
*/
    #endregion

    #region Código de colocación de grupos pares

        /*
    //Sabemos que el grupo es de 4 o 2, los cuales tienen colocación diferente del resto de grupos
    void ColocarCuadrilla<T>(List<T> lista, T[][] formacion, int numVanguardia, List<PosicionCombate> posiciones, PosicionCombate[][] arrayPosicionesVisuales) where T : Stats
    {

        bool duo = lista.Count == 2;

        //Equipo equilibrado
        if (numVanguardia == 2)
        {
            //Resultado (X = personaje)
            //-X-X-       -X-X-
            //-X-X-  OR   -----
            
            ColocarPersonaje(lista[0], formacion, 0, 1, posiciones, arrayPosicionesVisuales);
            ColocarPersonaje(lista[1], formacion, 0, 3, posiciones, arrayPosicionesVisuales);

            if (!duo)
            { //Hay gente en retaguardia
                ColocarPersonaje(lista[2], formacion, 1, 1, posiciones, arrayPosicionesVisuales);
                ColocarPersonaje(lista[3], formacion, 1, 3, posiciones, arrayPosicionesVisuales);
            }

        }
        else if (numVanguardia == 1) //Equipo con sólo 1 en vanguardia
        {
            //Resultado (X = personaje)
            //--X--       --X--
            //-XXX-  OR   --X--
            
            ColocarPersonaje(lista[0], formacion, 0, 2, posiciones, arrayPosicionesVisuales);

            if (duo)
            {
                ColocarPersonaje(lista[1], formacion, 1, 2, posiciones, arrayPosicionesVisuales);
            }
            else
            {
                ColocarPersonaje(lista[1], formacion, 1, 1, posiciones, arrayPosicionesVisuales);
                ColocarPersonaje(lista[2], formacion, 1, 2, posiciones, arrayPosicionesVisuales);
                ColocarPersonaje(lista[3], formacion, 1, 3, posiciones, arrayPosicionesVisuales);
            }
        }
        else
        {
            //Resultado (X = personaje)
            //-XXX-
            //--X--  OR   ERROR
            //
            //En este caso no puede ser un duo ya que no pueden haber una
            //cantidad de personajes diferente de 1 o 2 en vanguardia de
            //un grupo de 2 personajes
            
            ColocarPersonaje(lista[0], formacion, 0, 1, posiciones, arrayPosicionesVisuales);
            ColocarPersonaje(lista[1], formacion, 0, 2, posiciones, arrayPosicionesVisuales);
            ColocarPersonaje(lista[2], formacion, 0, 3, posiciones, arrayPosicionesVisuales);
            ColocarPersonaje(lista[3], formacion, 1, 2, posiciones, arrayPosicionesVisuales);
        }
    }
*/

    #endregion
}
