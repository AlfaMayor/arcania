﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelItem : MonoBehaviour
{

    public Image imagen;
    public Text texto;

    public Item item;

    public void SetItem(Item i)
    {
        item = i;
        imagen.sprite = i.sprite;
        texto.text = i.nombre;
    }

}
