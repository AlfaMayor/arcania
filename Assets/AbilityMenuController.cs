﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityMenuController : MonoBehaviour
{
    private MenuController menu;
    public CharBtnController[] btnChar;
    public Color orange, normal;
    public GameObject ability, spell, specials, spellContainer, abilityContainer;
    List<GameObject> abilities = new List<GameObject>();
    List<GameObject> spells = new List<GameObject>();
    private void Awake()
    {
        menu = MenuController.singleton;

        for (int i = 0; i < btnChar.Length; i++)
        {
            btnChar[i].assignCharacter(menu.characters[i]);
        }
    }
    public void showAbilities(int pos)
    {
        clearAbilities();
        specials.SetActive(true);
        changeColor();
        btnChar[pos].GetComponent<Image>().color = orange;
        Character c = menu.characters[pos];
        List<Skill> skills = c.skills;

        foreach (var skill in skills)
        {
            if (skill.type == TypeOfSkill.ABILITY)
            {
                var a = Instantiate(ability, abilityContainer.transform);
                abilities.Add(a);
                a.transform.GetChild(0).GetComponent<Text>().text = skill.nombre;
                a.transform.GetChild(1).GetComponent<Text>().text = skill.consumo.ToString();
            }
            else if (skill.type == TypeOfSkill.SPELL)
            {
                var s = Instantiate(spell, spellContainer.transform);
                abilities.Add(s);
                s.transform.GetChild(0).GetComponent<Text>().text = skill.nombre;
                s.transform.GetChild(1).GetComponent<Text>().text = skill.consumo.ToString();
            }
        }
    }

    private void clearAbilities()
    {
        while (abilities.Count > 0)
        {
            var a = abilities[0];
            abilities.RemoveAt(0);
            Destroy(a);
        }

        while (spells.Count > 0)
        {
            var s = abilities[0];
            abilities.RemoveAt(0);
            Destroy(s);
        }
    }

    private void changeColor()
    {
        for (int i = 0; i < btnChar.Length; i++)
        {
            btnChar[i].GetComponent<Image>().color = normal;
        }
    }
}
