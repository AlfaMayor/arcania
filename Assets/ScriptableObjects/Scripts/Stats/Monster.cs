﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Monster", menuName = "Scriptable Objects/Monster")]
public class Monster : Stats
{
    public Reward[] rewardTypes;
    [Min(0)]
    public int minGold;
    [Min(0)]
    public int maxGold;
    public ItemDrop[] itemRewards;
    public List<int> zonas = new List<int>(new int[] { 1 });

    public Monster SetStats(ReferenciaMonster referencia)
    {
        base.SetStats(referencia);

        return this;
    }
}
public enum Reward
{
    ITEM, GOLD, EXP
}

[Serializable]
public class ItemDrop
{
    public ReferenciaItem item;
    [Range(0, 1)] //0 = Nunca, 1 = Siempre
    public float chance;
}

