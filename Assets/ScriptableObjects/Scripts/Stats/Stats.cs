﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class Stats : ScriptableObject, IComparable<Stats>
{
    public string id;
    public string nombre;
    public Sprite sprite;
    public int hp;
    public int hpMax;
    public int mp;
    public int mpMax;
    public int energy;
    public int maxEnergy;
    public int atk;
    public int def;
    public int magicAtk;
    public int magicDef;
    public int agility;
    public int evasion;
    public int accuracy;
    public int luck;
    public int exp;

    public Affinities[] affinities = new Affinities[7] {
        new Affinities() { element = Element.AQUA, effect=AffinityType.RESIST,multiplier=AffinityEffect.NEUTRAL },
        new Affinities() { element = Element.FLAM, effect=AffinityType.RESIST,multiplier=AffinityEffect.NEUTRAL },
        new Affinities() { element = Element.FRIGO, effect=AffinityType.RESIST,multiplier=AffinityEffect.NEUTRAL },
        new Affinities() { element = Element.VIS, effect=AffinityType.RESIST,multiplier=AffinityEffect.NEUTRAL },
        new Affinities() { element = Element.VITA, effect=AffinityType.RESIST,multiplier=AffinityEffect.NEUTRAL },
        new Affinities() { element = Element.MORT, effect=AffinityType.RESIST,multiplier=AffinityEffect.NEUTRAL },
        new Affinities() { element = Element.NON_ELEMENTAL, effect=AffinityType.RESIST,multiplier=AffinityEffect.NEUTRAL }
    };

    public List<Skill> skills;

    public RuntimeAnimatorController animatorCombate;

    public int CompareTo(Stats s)
    {
        int dif = -agility.CompareTo(s.agility);

        if (dif == 0)
        {
            return UnityEngine.Random.Range(0, 2);
        }
        else
        {
            return dif;
        }
    }

    protected T SetStats<T>(ReferenciaStats<T> referencia) where T : Stats
    {

        hp = referencia.hp;
        hpMax = referencia.hpMax;
        mp = referencia.mp;
        mpMax = referencia.mpMax;
        energy = referencia.energy;
        maxEnergy = referencia.maxEnergy;
        atk = referencia.atk;
        def = referencia.def;
        magicAtk = referencia.magicAtk;
        magicDef = referencia.magicDef;
        agility = referencia.agility;
        evasion = referencia.evasion;
        accuracy = referencia.accuracy;
        luck = referencia.luck;
        exp = referencia.exp;

        return this as T;
    }

    public virtual StatBonus GetExtraStats()
    {
        return new StatBonus();
    }

}

[Serializable]
public class Affinities
{

    public Element element;
    public AffinityType effect = AffinityType.RESIST;
    /// <summary>
    /// Multiplicador de daño
    /// </summary>
    public AffinityEffect multiplier = AffinityEffect.NEUTRAL;
}

public enum AffinityEffect
{
    WEAK = 4, NEUTRAL = 2, STRONG = 1
}


public enum AffinityType
{
    RESIST, ABSORB
}

