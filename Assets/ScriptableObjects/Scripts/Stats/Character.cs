﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "Scriptable Objects/Character")]
public class Character : Stats
{
    public int lvl;
    public string clase;
    public RequiredClass tipoClase;
    public Necklace neck;
    public Weapon weapon;
    public Ring ring;
    public Helmet helmet;
    public Armor armor;
    public Legs legs;
    public Boots boots;
    public Coat coat;
    public Shield shield;
    public Light light;
    public int expToNextLvl;
    public int lvlPoints;
    public StatBonus levelReward;

    public virtual void LvlUp()
    {
        if (this.lvl < 100)
        {
            this.lvl++;
        }
        else
        {
            lvlPoints++;
        }

        this.expToNextLvl = calculateExp();
        this.hp += levelReward.hp;
        this.hpMax += levelReward.hp;
        this.mp += levelReward.mp;
        this.mpMax += levelReward.mp;
        this.energy += levelReward.ep;
        this.maxEnergy += levelReward.ep;
        this.atk += levelReward.atk;
        this.def += levelReward.def;
        this.magicAtk += levelReward.mAt;
        this.magicDef += levelReward.mDf;
        this.agility += levelReward.agl;
        this.evasion += levelReward.eva;
        this.accuracy += levelReward.acc;
        this.luck += levelReward.lck;

    }

    private int calculateExp()
    {
        throw new NotImplementedException();
    }

    public List<Equipment> GetEquipment()
    {
        List<Equipment> lista = new List<Equipment>()
        { neck, weapon, ring, helmet, armor, legs, boots, coat, shield, light };

        return lista;
    }

    public override StatBonus GetExtraStats()
    {
        StatBonus extra = new StatBonus();

        foreach (var eq in GetEquipment())
        {
            if (eq == null)
            {
                //SI EQ EN ESTE SLOT ES NULL PASA AL SIGUIENTE OBJETO
                continue;
            }

            extra.hp += eq.stats.hp;
            extra.mp += eq.stats.mp;
            extra.ep += eq.stats.ep;
            extra.atk += eq.stats.atk;
            extra.def += eq.stats.def;
            extra.mAt += eq.stats.mAt;
            extra.mDf += eq.stats.mDf;
            extra.agl += eq.stats.agl;
            extra.eva += eq.stats.eva;
            extra.acc += eq.stats.acc;
            extra.lck += eq.stats.lck;
        }

        return extra;
    }

    public Character SetStats(ReferenciaCharacter referencia)
    {
        base.SetStats(referencia);

        ItemList il = ItemList.Singleton;

        lvl = referencia.lvl;
        clase = referencia.clase;
        tipoClase = referencia.tipoClase;
        expToNextLvl = referencia.expToNextLvl;
        lvlPoints = referencia.lvlPoints;
        levelReward = referencia.lr;

        boots = il.GetItemOfType(referencia.boots);
        coat = il.GetItemOfType(referencia.coat);
        helmet = il.GetItemOfType(referencia.helmet);
        legs = il.GetItemOfType(referencia.legs);
        light = il.GetItemOfType(referencia.light);
        neck = il.GetItemOfType(referencia.neck);
        ring = il.GetItemOfType(referencia.ring);
        shield = il.GetItemOfType(referencia.shield);
        weapon = il.GetItemOfType(referencia.weapon);

        return this;
    }
}
