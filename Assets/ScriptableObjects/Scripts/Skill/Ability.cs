﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ability", menuName = "Scriptable Objects/Skill/Ability")]
public class Ability : Skill
{
    public Ability()
    {
        type = TypeOfSkill.ABILITY;
    }
}
