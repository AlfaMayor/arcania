﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill : ScriptableObject
{
    public string nombre;
    public int consumo;
    public Element[] elements = new Element[1];
    public TypeOfSkill type;
    public Area area;
    public Target objetivo;
    public float multiplier = 1;
}
public enum TypeOfSkill
{
    ABILITY, SPELL, NORMAL
}
public enum Target
{
    ENEMIES, ALLIES, ALL
}

public enum Area
{
    SINGLE, ALL, RANDOM
}

public enum Element
{
    AQUA = 0,
    FLAM = 1,
    FRIGO = 2,
    VIS = 3,
    VITA = 4,
    MORT = 5,
    NON_ELEMENTAL = 6
}
