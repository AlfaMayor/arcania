﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Spell", menuName = "Scriptable Objects/Skill/Spell")]
public class Spell : Skill
{
    public Spell()
    {
        type = TypeOfSkill.SPELL;
    }
}
