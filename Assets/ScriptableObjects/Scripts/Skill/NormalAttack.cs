﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class NormalAttack : Skill
{
    private static NormalAttack singleton;

    public static NormalAttack Singleton
    {
        get
        {
            if (singleton == null)
            {
                singleton = CreateInstance<NormalAttack>();
            }
            return singleton;
        }
    }

    public NormalAttack()
    {
        nombre = "Ataque Básico";
        consumo = 0;
        elements = new[] {Element.NON_ELEMENTAL};
        type = TypeOfSkill.NORMAL;
        area = Area.SINGLE;
        objetivo = Target.ENEMIES;
        multiplier = 1f;
    }
}
