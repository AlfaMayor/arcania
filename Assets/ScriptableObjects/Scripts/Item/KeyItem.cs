﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Key Item", menuName = "Scriptable Objects/Item/Key Item")]
public class KeyItem : Item
{
}
