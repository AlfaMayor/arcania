﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Consumable", menuName = "Scriptable Objects/Item/Consumable")]
public class Consumable : Item
{
    public Area area = Area.SINGLE;
    public Target objetivo = Target.ALLIES;

    public List<ConsumableEffect> efectos = new List<ConsumableEffect>();

    public void Uso(Stats target)
    {
        int intTipoResultado = 0;
        string resultado = "";
        foreach (var efecto in efectos)
        {
            intTipoResultado += (int)efecto.type;
            string tmp = efecto.ApplyEffect(target);
            if (tmp.Length > 0)
            {
                if (resultado.Length > 0) resultado += "\n"; //Cambia de línea
                resultado += tmp;
            }
        }
        var combate = CombatSceneController.Singleton;
        if (combate != null)
        {
            BuffOrDebuff tipoResultado;
            if (intTipoResultado < 0) tipoResultado = BuffOrDebuff.Heal;
            else if (intTipoResultado > 0) tipoResultado = BuffOrDebuff.Damage;
            else tipoResultado = BuffOrDebuff.Neutral;

            combate.TriggerShowChanges(target, tipoResultado, resultado);
        }
        
    }

    public void Consumir(Stats target)
    {
        Uso(target);
        cantidad--;
        if (cantidad <= 0)
        {
            var datos = DatosController.Singleton;
            datos.currentSave.inventario.todosLosItems.Remove(this);
        }
    }

    internal void Consumir(List<Stats> targets)
    {
        foreach (var target in targets)
        {
            Uso(target);
        }
        cantidad--;
        if (cantidad <= 0)
        {
            var datos = DatosController.Singleton;
            datos.currentSave.inventario.todosLosItems.Remove(this);
        }
    }
}
