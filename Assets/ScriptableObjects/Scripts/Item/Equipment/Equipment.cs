﻿using System.Collections;
using System.Collections.Generic;

//CLASE EQUIPMENT ES GENERICA (DEPENDE DE ITEM) DESCIENDE DE ITEM(DE TIPO EQUIPO) 
public abstract class Equipment : Item
{
    public StatBonus stats;
    public Affinities[] af;
    public RequiredClass[] reqClass;
}



public enum RequiredClass
{
    ARCHER, WARRIOR, HALF_BLOOD, MAGE, BERSERKER, SQUIRE, SPECIALIST
}
