﻿using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Scriptable Objects/Item/Equipment/Weapon")]
public class Weapon : Equipment
{
    public bool twoHanded, range;
}
