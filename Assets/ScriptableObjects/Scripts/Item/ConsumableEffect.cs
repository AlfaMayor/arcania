﻿using UnityEngine;

public abstract class ConsumableEffect : ScriptableObject
{
    public BuffOrDebuff type = BuffOrDebuff.Neutral;

    /// <summary>
    /// Cómo afecta a todo tipo de personajes
    /// </summary>
    /// <param name="target">El personaje afectado</param>
    public virtual string ApplyEffect(Stats target)
    {
        string result = "";
        if (target is Character character) result = ApplyEffect(character);
        else if (target is Monster monster) result = ApplyEffect(monster);

        return result;
    }

    /// <summary>
    /// Cómo afecta el consumible a un aliado específicamente
    /// </summary>
    /// <param name="target">El aliado afectado</param>
    protected abstract string ApplyEffect(Character target);

    /// <summary>
    /// Cómo afecta el consumible a un enemigo específicamente
    /// </summary>
    /// <param name="target">El enemigo afectado</param>
    protected abstract string ApplyEffect(Monster target);

}

public enum BuffOrDebuff
{
    Heal = -1, Neutral = 0, Damage = 1, CritDamage = 2
}