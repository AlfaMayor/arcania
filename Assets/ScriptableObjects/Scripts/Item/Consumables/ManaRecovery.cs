﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Mana Recovery", menuName = "Scriptable Objects/Item/Consumable Effect/Mana Recovery")]
public class ManaRecovery : ConsumableEffect
{
    [Min(0)]
    public int mpRecovery = 0;

    public override string ApplyEffect(Stats target)
    {
        string result = base.ApplyEffect(target);

        //Recupera la cantidad definida por el objeto o la diferencia entre MP actual y máximo, lo que sea menor
        int mpRecuperado = (target.mpMax - target.mp < mpRecovery) ? target.mpMax - target.mp : mpRecovery;

        target.mp += mpRecuperado;

        if (target.mp > target.mpMax) target.mp = target.mpMax;

        Debug.Log(target.nombre + " ha recuperado " + mpRecuperado + " MP");

        result += mpRecuperado + " MP";

        return result;
    }

    protected override string ApplyEffect(Character target)
    {
        return "";
    }

    protected override string ApplyEffect(Monster target)
    {
        return "";
    }

}
