﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Heal", menuName = "Scriptable Objects/Item/Consumable Effect/Heal")]
public class Heal : ConsumableEffect
{
    [Min(0)]
    public int hpRecovery = 0;

    public override string ApplyEffect(Stats target)
    {
        string result = base.ApplyEffect(target);

        //Recupera la cantidad definida por el objeto o la diferencia entre HP actual y máximo, lo que sea menor
        int hpRecuperado = (target.hpMax - target.hp < hpRecovery) ? target.hpMax - target.hp : hpRecovery;

        target.hp += hpRecuperado;

        if (target.hp > target.hpMax) target.hp = target.hpMax;

        Debug.Log(target.nombre + " ha recuperado " + hpRecuperado + " HP");

        result += hpRecuperado + " HP";

        return result;
    }

    protected override string ApplyEffect(Character target)
    {
        return "";
    }

    protected override string ApplyEffect(Monster target)
    {
        return "";
    }

}
