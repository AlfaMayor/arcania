﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase base sin genérico para poder guardar cualquier tipo de Item en una variable sin necesidad de hacer la clase entera genérica sólo por ese objeto
/// </summary>
public abstract class Item : ScriptableObject
{
    public Type tipo;
    public string id;
    public string nombre;
    public string descripcion;
    public int cantidad = 1;
    public int price;
    public Sprite sprite;

}
