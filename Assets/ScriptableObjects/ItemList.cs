﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "Lista de objetos", menuName = "Scriptable Objects/Listas/Item")]
public class ItemList : ScriptableObject
{

    private static ItemList singleton;

    public static ItemList Singleton
    {
        get
        {
            if (singleton == null)
            {
                singleton = Resources.Load<ItemList>("Lista de objetos");
            }
            return singleton;
        }
    }

    public Item plantillaErrores;

    [SerializeField] private List<Item> listaPlantillas = new List<Item>();

    private Item GetItemError(ReferenciaItem referencia)
    {
        Item item = Instantiate(plantillaErrores);
        item.id = referencia.id;
        item.cantidad = referencia.cantidad;
        item.descripcion += " (este es un objeto representativo de otro que no se encuentra en el juego. El ID es \"" + referencia.id + "\")";
        return item;
    }

    /// <summary>
    /// Devuelve una copia del objeto correspondiente
    /// </summary>
    /// <typeparam name="T">La clase del objeto (el tipo devuelto)</typeparam>
    /// <param name="referencia">La referencia del objeto original</param>
    /// <returns></returns>
    public Item GetItemFromReference(ReferenciaItem referencia)
    {
        Item temp = null;

        foreach (var item in listaPlantillas)
        {
            if (item.id.Equals(referencia.id))
            {
                temp = Instantiate(item);
                temp.cantidad = referencia.cantidad;
            }
        }
        if (temp == null)
        {
            temp = GetItemError(referencia);
        }

        return temp;
    }

    public T GetItemOfType<T>(ReferenciaItem<T> referencia) where T : Item
    {
        if (referencia == null) return null;

        T temp = null;

        foreach (var item in listaPlantillas)
        {
            if (item is T && item.id.Equals(referencia.id))
            {
                temp = Instantiate(item as T);
                temp.cantidad = referencia.cantidad;
            }
        }

        return temp;
    }

    public List<Item> GetItemsFromReference(List<ReferenciaItem> refInventario)
    {
        List<Item> lista = new List<Item>();

        foreach (var item in refInventario)
        {
            Item i = GetItemFromReference(item);
            if (i != null) lista.Add(i);
        }
        return lista;
    }

    public List<ReferenciaItem> GetReferenciasFromItems(List<Item> listaItems)
    {
        List<ReferenciaItem> listaRefs = new List<ReferenciaItem>();

        foreach (var item in listaItems)
        {
            listaRefs.Add(GetReferenciaFromItem(item));
        }
        return listaRefs;
    }

    public ReferenciaItem<T> GetReferenciaFromItem<T>(T item) where T : Item
    {
        return item == null ? null : new ReferenciaItem<T>() { id = item.id, cantidad = item.cantidad };
    }
}
