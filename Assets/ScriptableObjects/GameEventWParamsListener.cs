﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventWParamsListener : MonoBehaviour
{
    public GameEventWParams gameEventP; //Event d'entrada

    public CustomUnityEvent responseP; //Event de sortida


    private void OnEnable()
    {
        gameEventP.SubscribeListener(this);
    }

    private void OnDisable()
    {
        gameEventP.UnSubscribeListener(this);
    }

    /// <summary>
    /// Llama a las funciones guardadas en el evento de Unity
    /// </summary>
    /// <param name="parametros">Parámetros a pasar a las funciones</param>
    public void OnEventRaised(EventParameters parametros)
    {
        responseP.Invoke(parametros);
    }
}

/// <summary>
/// UnityEvent con parámetros. Los parámetros se guardan dentro de la variable de tipo EventParameters
/// </summary>
[Serializable]
public class CustomUnityEvent : UnityEvent<EventParameters>
{
    public EventParameters p;
}

/// <summary>
/// Contenedor de parámetros para eventos de tipo GameEventWParams
/// </summary>
[Serializable]
public class EventParameters
{
    public FormacionEquipo<Monster> paramEnemigos;

    public GameEventWParams eventoOrigen = null;

    public Vector2 paramVector2 = Vector2.zero;
    public float paramFloat = 0f;
    public int paramInt = 0;
    public string paramString = "";
}
