﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Datos de dialogos",menuName ="Scriptable Objects/DialogueData")]
public class DialogueData : ScriptableObject
{
    public List<Dialogo> Dialogos = new List<Dialogo>();

}
