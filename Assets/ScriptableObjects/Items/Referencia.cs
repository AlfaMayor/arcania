﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Las clases contenidas en este script NO SON SCRIPTABLE OBJECTS

/// <summary>Clase para guardar referencias a plantillas guardadas como ScriptableObject
/// El genérico <typeparamref name="T"/> indica la clase base del objeto del cual es una plantilla.</summary>
/// <typeparam name="T">Clase de la plantilla (por ejemplo <see cref="Equipment"/>, que deriva de <see cref="Item{T}"/></typeparam>
[Serializable]
public class Referencia<T> where T : ScriptableObject
{
    public string id;
}

/// <summary>Base de referencia para objetos. En la estructura de objetos seria equivalente a la clase <see cref="Item"/> (sin genéricos)</summary>
[Serializable]
public class ReferenciaItem : Referencia<Item>
{
    public int cantidad;
}

/// <summary>Base genérica para objetos. Se usa por ejemplo a la hora de recuperar el equipamiento de los personajes, para que no haga falta primero añadirlo al inventario y luego pasarlo de ahi al personaje</summary>
/// <typeparam name="T">El tipo específico de objeto</typeparam>
[Serializable]
public class ReferenciaItem<T> : ReferenciaItem where T : Item
{
}

[Serializable]
public abstract class ReferenciaStats<T> : Referencia<T> where T : Stats
{
    public string nombre;
    public int hp;
    public int hpMax;
    public int mp;
    public int mpMax;
    public int energy;
    public int maxEnergy;
    public int atk;
    public int def;
    public int magicAtk;
    public int magicDef;
    public int agility;
    public int evasion;
    public int accuracy;
    public int luck;
    public int exp;
    public List<Skill> skills = new List<Skill>();

    public ReferenciaStats<T> SetStats(T personaje)
    {
        nombre = personaje.nombre;
        hp = personaje.hp;
        hpMax = personaje.hpMax;
        mp = personaje.mp;
        mpMax = personaje.mpMax;
        energy = personaje.energy;
        maxEnergy = personaje.maxEnergy;
        atk = personaje.atk;
        def = personaje.def;
        magicAtk = personaje.magicAtk;
        magicDef = personaje.magicDef;
        agility = personaje.agility;
        evasion = personaje.evasion;
        accuracy = personaje.accuracy;
        luck = personaje.luck;
        exp = personaje.exp;
        skills = personaje.skills;

        return this;

    }

}

/// <summary>Guarda el personaje con el equipamiento como ReferenciaItem</summary>
[Serializable]
public class ReferenciaCharacter : ReferenciaStats<Character>
{
    public int lvl;
    public string clase;
    public RequiredClass tipoClase;
    public ReferenciaItem<Necklace> neck;
    public ReferenciaItem<Weapon> weapon;
    public ReferenciaItem<Ring> ring;
    public ReferenciaItem<Helmet> helmet;
    public ReferenciaItem<Armor> armor;
    public ReferenciaItem<Legs> legs;
    public ReferenciaItem<Boots> boots;
    public ReferenciaItem<Coat> coat;
    public ReferenciaItem<Shield> shield;
    public ReferenciaItem<Light> light;
    public int expToNextLvl;
    public int lvlPoints;
    public StatBonus lr;

    public new ReferenciaCharacter SetStats(Character character)
    {
        base.SetStats(character);

        ItemList il = ItemList.Singleton;

        id = character.id;
        lvl = character.lvl;
        clase = character.clase;
        tipoClase = character.tipoClase;
        expToNextLvl = character.expToNextLvl;
        lvlPoints = character.lvlPoints;
        lr = character.levelReward;

        if (character.boots != null) boots = il.GetReferenciaFromItem(character.boots);
        if (character.coat != null) coat = il.GetReferenciaFromItem(character.coat);
        if (character.helmet != null) helmet = il.GetReferenciaFromItem(character.helmet);
        if (character.legs != null) legs = il.GetReferenciaFromItem(character.legs);
        if (character.light != null) light = il.GetReferenciaFromItem(character.light);
        if (character.neck != null) neck = il.GetReferenciaFromItem(character.neck);
        if (character.ring != null) ring = il.GetReferenciaFromItem(character.ring);
        if (character.shield != null) shield = il.GetReferenciaFromItem(character.shield);
        if (character.weapon != null) weapon = il.GetReferenciaFromItem(character.weapon);

        return this;
    }
}
public class ReferenciaMonster : ReferenciaStats<Monster>
{
    public new ReferenciaMonster SetStats(Monster monster)
    {
        base.SetStats(monster);

        return this;
    }
}