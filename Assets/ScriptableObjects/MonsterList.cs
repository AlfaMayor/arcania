﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Lista de Monsters", menuName = "Scriptable Objects/Listas/Monster")]
public class MonsterList : ScriptableObject
{

    private static MonsterList singleton;

    public static MonsterList Singleton
    {
        get
        {
            if (singleton == null)
            {
                singleton = Resources.Load<MonsterList>("MonsterList");
            }
            return singleton;
        }
    }

    public Monster plantillaError;

    [SerializeField] private List<Monster> listaPlantillas = new List<Monster>();

    private Monster GetMonsterError(ReferenciaMonster referencia)
    {
        Monster monstruo = Instantiate(plantillaError).SetStats(referencia);
        monstruo.id = referencia.id;
        return monstruo;
    }

    /// <summary>
    /// Devuelve una copia del personaje
    /// </summary>
    /// <param name="referencia"></param>
    /// <returns></returns>
    public Monster GetMonsterFromReference(ReferenciaMonster referencia)
    {
        Monster temp = null;

        foreach (var item in listaPlantillas)
        {
            if (item.id.Equals(referencia.id))
            {
                temp = Instantiate(item);

            }
        }
        if (temp == null)
        {
            temp = GetMonsterError(referencia);
        }

        return temp;
    }

    public List<Monster> GetMonstersFromReference(List<ReferenciaMonster> referencias)
    {
        List<Monster> lista = new List<Monster>();

        foreach (var referencia in referencias)
        {
            Monster personaje = GetMonsterFromReference(referencia);
            if (personaje != null) lista.Add(personaje);
        }
        return lista;
    }

    public List<ReferenciaMonster> GetReferenciasFromMonsters(List<Monster> listaMonster)
    {
        List<ReferenciaMonster> listaRefs = new List<ReferenciaMonster>();

        foreach (var item in listaMonster)
        {
            listaRefs.Add(GetReferenciaFromMonster(item));
        }
        return listaRefs;
    }

    public ReferenciaMonster GetReferenciaFromMonster(Monster Monster)
    {
        ReferenciaMonster referencia = new ReferenciaMonster().SetStats(Monster);

        return referencia;
    }

    internal FormacionEquipo<Monster> GetRandomMonsterTeam(int currentArea)
    {
        List<Monster> listaCandidatos = listaPlantillas.Where(x => x.zonas.Contains(currentArea)).ToList();
        int cantidad = UnityEngine.Random.Range(1, 6); //Número del 1 al 5 (ambos incluidos)

        List<Monster> resultado = new List<Monster>();

        for (int i = 0; i < cantidad; i++)
        {
            resultado.Add(Instantiate(listaCandidatos[UnityEngine.Random.Range(0, listaCandidatos.Count)])); //Añade monstruo aleatorio
        }

        return new FormacionEquipo<Monster>(resultado, UnityEngine.Random.Range(1, 4));


    }
}
