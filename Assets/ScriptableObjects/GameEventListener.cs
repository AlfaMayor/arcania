﻿using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    public GameEvent gameEvent;

    public UnityEvent response;

    private void OnEnable()
    {
        gameEvent.SubscribeListener(this);
    }

    private void OnDisable()
    {
        gameEvent.UnSubscribeListener(this);
    }

    public virtual void OnEventRaised()
    {
        response.Invoke();
    }
}
