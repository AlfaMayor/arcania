﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Lista de Characters", menuName = "Scriptable Objects/Listas/Character")]
public class CharacterList : ScriptableObject
{

    private static CharacterList singleton;

    public static CharacterList Singleton
    {
        get
        {
            if (singleton == null)
            {
                singleton = Resources.Load<CharacterList>("CharactersList");
            }
            return singleton;
        }
    }

    public Character plantillaMC;

    public Character PlantillaMC
    {
        get => Instantiate(plantillaMC);
    }

    public Character plantillaError;

    [SerializeField] private List<Character> listaPlantillas = new List<Character>();

    private Character GetCharacterError(ReferenciaCharacter referencia)
    {
        Character personaje = Instantiate(plantillaError).SetStats(referencia);
        personaje.id = referencia.id;
        return personaje;
    }

    /// <summary>
    /// Devuelve una copia del personaje
    /// </summary>
    /// <param name="referencia"></param>
    /// <returns></returns>
    public Character GetCharacterFromReference(ReferenciaCharacter referencia)
    {
        Character temp = null;

        foreach (var item in listaPlantillas)
        {
            if (item.id.Equals(referencia.id))
            {
                temp = Instantiate(item).SetStats(referencia);

            }
        }
        if (temp == null)
        {
            temp = GetCharacterError(referencia);
        }

        return temp;
    }

    public List<Character> GetCharactersFromReference(List<ReferenciaCharacter> referencias)
    {
        List<Character> lista = new List<Character>();

        foreach (var referencia in referencias)
        {
            Character personaje = GetCharacterFromReference(referencia);
            if (personaje != null) lista.Add(personaje);
        }
        return lista;
    }

    public List<ReferenciaCharacter> GetReferenciasFromCharacters(List<Character> listaCharacter)
    {
        List<ReferenciaCharacter> listaRefs = new List<ReferenciaCharacter>();

        foreach (var item in listaCharacter)
        {
            listaRefs.Add(GetReferenciaFromCharacter(item));
        }
        return listaRefs;
    }

    public ReferenciaCharacter GetReferenciaFromCharacter(Character character)
    {
        ReferenciaCharacter referencia = new ReferenciaCharacter().SetStats(character);

        return referencia;
    }

}
