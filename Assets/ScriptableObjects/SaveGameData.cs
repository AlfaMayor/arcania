﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using UnityEngine;

//TODO Limpieza y comentario

[CreateAssetMenu(fileName = "PlantillaSave", menuName = "Scriptable Objects/Plantilla SaveGameData")]
public class SaveGameData : ScriptableObject
{
    /// <summary>
    /// Ruta de la carpeta donde se guardan las partidas
    /// </summary>
    private static string savePath = "/save_data/";

    private static string SavePath { get => Application.persistentDataPath + savePath; }
    /// <summary>
    /// Nombre del archivo de guardado sin el número
    /// </summary>
    private static string fileName = "save";
    private static string extension = ".txt";

    private static string GetSaveFileFullPath(int id)
    {
        return string.Format(SavePath + fileName + "{0}" + extension, id.ToString());
    }

    private static SaveGameData template;
    /// <summary>
    /// Devuelve una copia de la plantilla de partida nueva
    /// </summary>
    public static SaveGameData TemplateCopy
    {
        get
        {
            if (template == null)
            {
                template = Resources.Load<SaveGameData>("PlantillaSave").Clone(true);
            }
            return template;
        }
    }

    /// <summary>
    /// Crea una instancia temporal de la partida guardada (esta instancia se elimina al cerrar el juego)
    /// </summary>
    /// <param name="useTemplate">Indica si vamos a usar la plantilla o si vamos a rellenar los datos manualmente</param>
    /// <returns></returns>
    public static SaveGameData CreateSaveObject(bool useTemplate = true)
    {
        var save = useTemplate ? TemplateCopy : CreateInstance<SaveGameData>();

        //Si no hay datos iniciales de partida crea una partida vacía y muestra error por consola (esto no debería pasar en el juego final)
        if (save == null)
        {
            save = CreateInstance<SaveGameData>();
            Debug.LogError("Ha habido un problema al cargar los datos iniciales de partida");
        }

        return save;
    }

    /// <summary>
    /// Guarda el objeto a un archivo
    /// </summary>
    /// <param name="save"></param>
    public static SaveGameData SaveToSlot(SaveGameData save = null)
    {
        if (save == null)
            save = CreateSaveObject();

        if (save.newGame)
        {
            save.saveId = GetAvailableId();
            save.newGame = false;
        }
        else
        {
            //Convierte todos los ScriptableObjects a referencias para ahorrar espacio en disco (no se guardan todos los datos, los que no cambian a lo largo del juego se cogen de una plantilla)
            save.PrepareForSerialization();
        }



        CheckSaveFolder();

        string fullPath = GetSaveFileFullPath(save.saveId);

        string json = JsonUtility.ToJson(save);

        string base64 = EncodeTo64(json);

        File.WriteAllText(fullPath, base64);

        return save;
    }

    /// <summary>
    /// Comprueba si existe la carpeta y la crea en caso de que no exista
    /// </summary>
    private static void CheckSaveFolder()
    {
        if (!Directory.Exists(SavePath))
            Directory.CreateDirectory(SavePath);
    }

    public static List<SaveGameData> GetSaveFiles()
    {
        CheckSaveFolder();
        DirectoryInfo d = new DirectoryInfo(SavePath);

        List<SaveGameData> saves = new List<SaveGameData>();

        FileInfo[] files = d.GetFiles();
        foreach (var file in files)
        {
            Match m = Regex.Match(file.Name, fileName + @"\d+" + extension);
            if (m.Success)
            {
                int id = Int32.Parse(Regex.Match(file.Name, @"\d+").Value);
                saves.Add(GetSaveFileFromSlot(id));
            }
        }

        return saves;
    }

    /// <summary>
    /// Obtiene el archivo de guardado elegido según el id
    /// </summary>
    /// <param name="path">Ruta del archivo con "{0}" en lugar del identificador (SaveGameData ya tiene constantes con esta ruta)</param>
    /// <returns></returns>
    public static SaveGameData GetSaveFileFromSlot(int id)
    {
        string fullPath = GetSaveFileFullPath(id);

        if (!File.Exists(fullPath))
        {
            Debug.LogWarning("El archivo de guardado no existe.");
            return null;
        }
        //Sacamos el archivo codificado del archivo
        string base64 = File.ReadAllText(fullPath);

        //Convertimos a JSON
        string json = DecodeFrom64(base64);

        //Cogemos una copia de la plantilla, por si se han perdido datos o ha habido una actualizacion que añade mas cosas que deben estar definidas por defecto
        SaveGameData save = TemplateCopy;

        JsonUtility.FromJsonOverwrite(json, save);


        //Por si ha cambiado el orden de los saves, esto se actualiza cuando cargamos partida
        //para que recuerde su slot (el slot se corresponde con el número en el archivo de guardado,
        //esto permite cambiar el orden de estos facilmente y recordar cual es su sitio a la hora de guardar
        save.saveId = id;

        return save;
    }

    private static int GetAvailableId()
    {
        CheckSaveFolder();
        DirectoryInfo d = new DirectoryInfo(SavePath);

        if (d.GetFiles().Length == 0) return 0;

        FileInfo[] files = d.GetFiles();
        List<int> freeSlots = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        foreach (var file in files)
        {
            Match m = Regex.Match(file.Name, fileName + @"\d+" + extension);
            if (m.Success)
            {
                //Buscamos el número y lo quitamos de la lista. Si el archivo no tiene número no hace nada
                int temp = Int32.Parse(Regex.Match(file.Name, @"\d+").Value); //TODO comprobar que SÓLO coge el número de archivos de guardado
                freeSlots.Remove(temp);
            }
        }

        int id = freeSlots.Count > 0 ? freeSlots[0] : -1;

        return id;
    }

    public static SaveGameData LoadFromSlot(int id)
    {
        if (!Directory.Exists(SavePath))
        {
            Debug.LogError("No hay archivos de guardado ni carpeta para estos, esta acción no deberia ser posible.");
            return null;
        }

        SaveGameData save = GetSaveFileFromSlot(id);

        if (save != null)
            LoadFromSave(save);


        return save;

    }

    public static SaveGameData LoadFromSave(SaveGameData save)
    {
        save.DeserializedAlready();
        return save;
    }

    #region Base64 Encode/Decode
    private static string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes
              = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue
              = System.Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    private static string DecodeFrom64(string encodedData)
    {
        byte[] encodedDataAsBytes
            = System.Convert.FromBase64String(encodedData);
        string returnValue =
           System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
        return returnValue;
    }
    #endregion

    #region Variables para serializado (no usables durante la partida)

    /// <summary>Guarda los cambios en características de los personajes del grupo</summary>
    public List<ReferenciaCharacter> refGrupo = new List<ReferenciaCharacter>();

    /// <summary>Guarda el indice en el que se guarda el personaje en cada una de sus posiciones</summary>
    [SerializeField]
    private List<int> refFormacion = new List<int>();

    /// <summary>La cantidad de personajes en la línea frontal de la formación</summary>
    [SerializeField, HideInInspector]
    private int numEnVanguardia;

    /// <summary>Guarda ID y cantidad de todos los objetos del inventario</summary>
    [SerializeField, HideInInspector]
    private List<ReferenciaItem> refInventario = new List<ReferenciaItem>();

    [SerializeField, HideInInspector]
    private int dinero = 0;


    #endregion

    #region Variables usables en partida

    public int saveId = 1;

    public bool newGame = true;

    public Vector2 playerPos = Vector2.zero;

    public int currentArea = -1;

    [NonSerialized]
    public Inventory inventario = new Inventory();

    [NonSerialized]
    public List<Character> personajesEquipo = new List<Character>();

    [NonSerialized]
    public FormacionEquipo<Character> formacion;

    #endregion

    /// <summary>
    /// Actualiza los campos que van a ser serializados con los datos correctos
    /// </summary>
    private void PrepareForSerialization()
    {
        //Preparar items para serializado
        ItemList il = ItemList.Singleton;
        Inventory inv = inventario;

        //Recupera la lista de items a partir de las plantillas predefinidas
        refInventario = il.GetReferenciasFromItems(inv.todosLosItems);

        dinero = inv.dinero;

        //Preparar equipo para serializado
        CharacterList cl = CharacterList.Singleton;

        //Nunca puedes tener menos de 1 personaje en el grupo (como puedes jugar sin personajes?)
        //Esto evita problemas mayores si por casualidad la plantilla de guardado tiene la lista de personajes vacía
        if (personajesEquipo.Count < 1) personajesEquipo.Add(cl.PlantillaMC);

        refGrupo = cl.GetReferenciasFromCharacters(personajesEquipo);

        //Preparar formación para serializado

        //Guarda qué personajes que tiene el jugador en el grupo están en
        //la formación y en qué orden (asegura que al cargar la partida sigan en el mismo orden)
        refFormacion = new List<int>();

        foreach (var personaje in formacion.lista)
        {
            int index = personajesEquipo.IndexOf(personaje);
            if (index >= 0) refFormacion.Add(index);
        }

        numEnVanguardia = formacion.numEnVanguardia;

    }

    /// <summary>
    /// Convierte los datos serializados en datos usables
    /// </summary>
    private void DeserializedAlready()
    {
        var datos = DatosController.Singleton;
        datos.currentSave = this;

        if (!newGame)
        {
            datos.overworldIniciado = true;
        }

        //Recupera los objetos a partir de las plantillas definidas.
        //Todos aquellos objetos que no tengan una plantilla con la que asignarse
        //serán sustituidas por un "objeto inútil"
        ItemList il = ItemList.Singleton;

        List<Item> items = il.GetItemsFromReference(refInventario);

        //Monta el inventario con los objetos recuperados y el dinero que tenía el jugador durante la última sesión
        inventario = new Inventory(items, dinero);

        //Recupera los miembros del equipo.
        //Igual que con los objetos, si no se encuentra la referencia del personaje 
        //dentro de la lista de plantillas, se devolverá uno marcado como "roto".
        //La diferencia en este caso es que mantiene sus estadísticas originales
        //a cambio de no tener habilidades ni afinidades 
        //(éstas no se guardan al ser constantes durante toda la partida)
        CharacterList cl = CharacterList.Singleton;

        personajesEquipo = cl.GetCharactersFromReference(refGrupo);

        //Construye la formación a partir de la lista de integrantes del grupo,
        //el número de personajes en la línea frontal, y el orden guardado anteriormente en una lista
        formacion = new FormacionEquipo<Character>(personajesEquipo, numEnVanguardia, refFormacion);

        //Liberamos espacio, ya que no necesitamos que estas variables estén asignadas durante la partida
        refInventario = null;
        refGrupo = null;
        refFormacion = null;


    }

    public SaveGameData Clone(bool newGame)
    {
        SaveGameData newSave = Instantiate(this);
        newSave.newGame = newGame;
        if (newGame)
            newSave.saveId = GetAvailableId();

        return newSave;
    }
}


