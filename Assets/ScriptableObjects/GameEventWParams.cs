﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase de eventos con parámetros
/// </summary>
[CreateAssetMenu(fileName = "GameEvent with parameters", menuName = "Scriptable Objects/Parameterized Game Event Object")]
public class GameEventWParams : ScriptableObject
{
    /// <summary>
    /// Lista de listeners a los que llamar al evocar el evento
    /// </summary>
    public List<GameEventWParamsListener> listeners = new List<GameEventWParamsListener>();

    /// <summary>
    /// Indica si el evento está activo o no (en el caso de que haya múltiples llamadas seguidas antes de llamar a Lower()).
    /// </summary>
    public bool isRaised = false;
    /// <summary>
    /// Indica si se tiene que ejecutar al cargar de nuevo el mapa.
    /// </summary>
    public bool delayed = false;

    /// <summary>
    /// Parámetros usados cuando se retrasa la activación del evento con Delay()
    /// </summary>
    public EventParameters delayedParams;

    /// <summary>
    /// Suscribe un listener
    /// </summary>
    /// <param name="listener">El listener a suscribir</param>
    public void SubscribeListener(GameEventWParamsListener listener)
    {
        listeners.Add(listener);
    }

    /// <summary>
    /// Desuscribe un listener
    /// </summary>
    /// <param name="listener">El listener a desuscribir</param>
    public void UnSubscribeListener(GameEventWParamsListener listener)
    {
        listeners.Remove(listener);
    }

    /// <summary>
    /// Prepara el evento para evocarlo automáticamente más tarde
    /// </summary>
    /// <param name="parametros"></param>
    public void Delay(EventParameters parametros)
    {
        delayed = true;
        delayedParams = parametros;
    }

    /// <summary>
    /// Llamada por delay. Si no se ha llamado antes a la función Delay() no hará nada.
    /// </summary>
    public GameEventWParams Raise()
    {
        if (delayed)
        {
            delayed = false;
            Raise(delayedParams);
        }
        return this;
    }
    /// <summary>
    /// Alza el evento
    /// </summary>
    /// <param name="parametros">Parámetros a pasar</param>
    public GameEventWParams Raise(EventParameters parametros)
    {
        return Raise(parametros, false);
    }
    /// <summary>
    /// Alza el evento
    /// </summary>
    /// <param name="parametros">Parámetros a pasar</param>
    /// <param name="forzar">Forzar el alzado aunque ya lo esté</param>
    public GameEventWParams Raise(EventParameters parametros, bool forzar)
    {
        if (isRaised && !forzar) return this; //Si esta actualmente activo evita lanzar otro "pulso" a menos de que se fuerce

        isRaised = true;
        foreach (var item in listeners) //Alerta a todos los listeners
        {
            item.OnEventRaised(parametros);
        }
        return this;
    }

    /// <summary>
    /// Permite volver a llamar a Raise() y devuelve delayedParams a null
    /// </summary>
    public void Lower()
    {
        isRaised = false;
        delayed = false;
        if (!delayed)
            delayedParams = null;
    }

    private void OnDisable()
    {
        Lower();
    }
}
