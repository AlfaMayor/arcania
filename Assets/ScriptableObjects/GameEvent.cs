﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "Scriptable Objects/Game Event Object", order = 0)]

public class GameEvent : ScriptableObject
{
    public List<GameEventListener> listeners = new List<GameEventListener>();
    public bool isRaised;
    public bool delayed;

    public void SubscribeListener(GameEventListener e) {
        listeners.Add(e);
    }

    public void UnSubscribeListener(GameEventListener e)
    {
        listeners.Remove(e);
    }

    public GameEvent Raise()
    {
        if (isRaised) return this;
        isRaised = true;

        foreach (var item in listeners)
        {
            item.OnEventRaised();
        }
        return this;
    }

    public void Lower()
    {
        isRaised = false;
    }

    private void OnDisable()
    {
        Lower();
    }



}
