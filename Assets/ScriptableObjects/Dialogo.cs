﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Dialogo", menuName ="Scriptable Objects/Dialogue")]
public class Dialogo : ScriptableObject
{

    public List<Sprite> SpriteList;
    public List<string> LineList;
    public List<Color> ColorList;

}
