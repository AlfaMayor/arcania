﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LoadLastFormation : MonoBehaviour
{
    public List<CharBtnController> listaVanguardia;
    public List<CharBtnController> listaRetaguardia;

    // Start is called before the first frame update
    void Start()
    {
        var formacion = DatosController.Singleton.currentSave.formacion;

        for (int i = 0; i < formacion.numEnVanguardia; i++)
        {
            listaVanguardia[i].assignCharacter(formacion.lista[i]);
        }
        for (int i = formacion.numEnVanguardia; i < formacion.lista.Count; i++)
        {
            listaRetaguardia[i - formacion.numEnVanguardia].assignCharacter(formacion.lista[i]);
        }
        var listaVacios = listaVanguardia.Where(x => x.personaje == null).ToList();
        listaVacios.AddRange(listaRetaguardia.Where(X => X.personaje == null));

        foreach (var item in listaVacios)
        {
            listaVanguardia.Remove(item);
            listaRetaguardia.Remove(item);
            item.gameObject.SetActive(false);
        }


    }
}
