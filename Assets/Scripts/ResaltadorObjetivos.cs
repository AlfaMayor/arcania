﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ResaltadorObjetivos : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
{
    public BotonMenuAux botonAux;

    private static CombatSceneController controladorCombate;

    private void Start()
    {
        if (controladorCombate == null) //No hace falta buscarlo cada vez
        {
            controladorCombate = FindObjectOfType<CombatSceneController>();
            if (controladorCombate == null) //Si no hay controlador de combate este script es inútil
            {
                Debug.LogError("No hay controlador de combate");
                Destroy(this);
            }
        }
        //controladorCombate.personajesEnElPuntoDeMira
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        controladorCombate.personajesEnElPuntoDeMira[botonAux.posicion].Highlight(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        controladorCombate.personajesEnElPuntoDeMira[botonAux.posicion].Highlight(false);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        controladorCombate.personajesEnElPuntoDeMira[botonAux.posicion].Highlight(false);
    }
}
