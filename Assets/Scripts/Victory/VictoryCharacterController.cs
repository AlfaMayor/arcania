﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryCharacterController : MonoBehaviour
{
    public Image face;
    public Text nombre, lvl, exp, expToNextLvl, newAbility;

    public void AssignVictoryCharacter(Character c)
    {
        face.sprite = c.sprite;
        nombre.text = c.nombre;
        lvl.text = c.lvl+"";
        exp.text = c.exp+"/";
        expToNextLvl.text = c.expToNextLvl+"";
        //añadir bool newAbility en Character para añadir en newAbility
    }
}
