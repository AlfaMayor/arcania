﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CharBtnController : MonoBehaviour
{
    public Image imagen;
    public Text nombre;
    public Character personaje;
    public int posicion;
    public GameEventWParams gewp;
    public EventParameters ep;

    public void assignCharacter(Character c, int pos = -1)
    {
        if (pos >= 0)
        {
            ep.paramInt = pos;
            posicion = pos;
        }
        personaje = c;
        if (c.sprite != null)
        {
            imagen.color = Color.white;
        }
        imagen.sprite = c.sprite;
        if (nombre != null)
        {
            nombre.text = c.nombre;
        }
    }

    public void assignButton()
    {
        if (!VarEstaticas.personajes.Contains(VarEstaticas.seleccion))
        {
            VarEstaticas.personajes[posicion] = VarEstaticas.seleccion;
            assignCharacter(VarEstaticas.seleccion);
            VarEstaticas.seleccion = null;
            gewp.Raise(null).Lower();
        }
    }

    public void Seleccionado()
    {
        VarEstaticas.seleccion = personaje;
        gewp.Raise(ep).Lower();
    }
}
