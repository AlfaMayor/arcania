﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayFormationController : MonoBehaviour
{
    public GameEventWParams gewp;

    public void AmpliarFormacion(int i)
    {
        EventParameters ep = new EventParameters();
        ep.paramInt = i;
        gewp.Raise(ep).Lower();
    }

    public void AssignNumVanguardia(int i)
    {
        VarEstaticas.numEnVanguardia = i;
    }
}