﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EqMenuController : MonoBehaviour
{
    private MenuController menu;
    public GameObject[] eqPanels;
    string[] nombres = new string[10] {"COLLARES","ARMAS","ANILLOS","CASCOS","ARMADURAS","PIERNAS","BOTAS","CAPAS","ESCUDOS","LUCES"};
    public Button[] eqButtons;
    public GameObject eqItem;
    public Text nombreItemsEq;
    public CharBtnController[] btnChar;
    public Color orange, normal;

    private void Awake()
    {
        menu = MenuController.singleton;
        for (int i = 0; i < btnChar.Length; i++)
        {
            btnChar[i].assignCharacter(menu.characters[i]);
        }
    }

    public void showItems(int type)
    {
        eqItem.SetActive(true);
        nombreItemsEq.text = nombres[type];
    }

    public void showPanel(int pos)
    {
        eqItem.SetActive(false);
        changeColor();
        btnChar[pos].GetComponent<Image>().color = orange;
        
        for (int i = 0; i < eqPanels.Length; i++)
        {
            eqPanels[i].SetActive(true);
        }

        var equipamiento = menu.characters[pos].GetEquipment();
        
        for (int i = 0; i < eqButtons.Length; i++)
        {
            Equipment e = equipamiento[i];
            Image buttonImage = eqButtons[i].transform.GetChild(0).GetComponent<Image>();
            buttonImage.enabled = true;
            if (e != null)
            {
                buttonImage.sprite = e.sprite;
            }
            else
            {
                buttonImage.enabled = false;
            }
        }
    }

    private void changeColor()
    {
        for (int i = 0; i < btnChar.Length; i++)
        {
            btnChar[i].GetComponent<Image>().color = normal;
        }
    }
}
