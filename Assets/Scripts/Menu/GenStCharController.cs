﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenStCharController : MonoBehaviour
{
    public Image imagen;
    public Text nombre;
    public Text clase;
    public Text hp;
    public Text lvl;
    public Text mp;
    public Text exp;
    public Text ep;
    public Text expToNextLvl;
    
    public void assignCharacter(Character c)
    {
        imagen.sprite = c.sprite;
        nombre.text = c.nombre;
        clase.text = c.clase;
        hp.text = "HP: "+c.hp + "/" + c.hpMax;
        lvl.text = "Lvl: "+c.lvl+"";
        mp.text = "MP: "+c.mp + "/" + c.mpMax;
        exp.text = "Exp: "+c.exp+"";
        ep.text = "EP: "+c.energy + "/" + c.maxEnergy;
        expToNextLvl.text = "NextLvl: "+c.expToNextLvl + "";
    }
}


