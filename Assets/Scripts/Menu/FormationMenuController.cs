﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FormationMenuController : MonoBehaviour
{
    private MenuController menu;

    //Lista de personajes
    public GameObject panelCharacters;
    public List<CharBtnController> btnChar;
    public CharBtnController btnCharPrefab;

    //Selector de tamaño de formación
    public Button[] size;

    //Prefabs de formaciones
    public GameObject[] display;
    public GameObject[] formationDisplay;

    public Button aceptar;
    public Image check;
    GameObject panelActivo = null;
    public Color red, white = Color.white, normal, orange;

    private void Awake()
    {
        menu = MenuController.singleton;
        var datos = DatosController.Singleton;
        int k = 0;

        foreach (var personaje in datos.currentSave.personajesEquipo)
        {
            var temp = Instantiate(btnCharPrefab, panelCharacters.transform);
            temp.assignCharacter(personaje, k);
            btnChar.Add(temp);
            k++;
        }
        panelActivo = Instantiate(display[5], transform.GetChild(1));
    }

    public void MostrarPanel(int pos)
    {
        aceptado();
        changeColor(null);
        panelCharacters.SetActive(false);
        VarEstaticas.formationSize = pos + 1;
        VarEstaticas.seleccion = null;
        for (int i = 0; i < VarEstaticas.personajes.Length; i++)
        {
            VarEstaticas.personajes[i] = null;
        }
        ClosePanel();
        size[pos].GetComponent<Image>().color = red;
        panelActivo = Instantiate(display[pos], transform.GetChild(1));
    }

    public void showCharacters()
    {
        panelCharacters.SetActive(true);
    }

    private void ClosePanel()
    {
        Destroy(panelActivo);
        check.gameObject.SetActive(false);
        for (int i = 0; i < size.Length; i++)
        {
            size[i].GetComponent<Image>().color = white;
        }
    }

    public void AmpliarFormacion(EventParameters ep)
    {
        int i = ep.paramInt;
        MostrarPanelFinal(i);
        showCharacters();
    }

    public void MostrarPanelFinal(int pos)
    {
        Destroy(panelActivo);
        panelActivo = Instantiate(formationDisplay[pos], transform.GetChild(1));
    }
    public void changeColor(EventParameters ep)
    {
        for (int i = 0; i < btnChar.Count; i++)
        {
            btnChar[i].GetComponent<Image>().color = normal;
        }
        if (ep != null)
        {
            btnChar[ep.paramInt].GetComponent<Image>().color = orange;
        }
        else
        {
            var array = VarEstaticas.personajes;
            if (array.Where(x => x != null).Count() == VarEstaticas.formationSize)
            {
                aceptar.gameObject.SetActive(true);
            }
            else
            {
                aceptar.gameObject.SetActive(false);
            }
        }
    }

    public void ConfirmarFormacion()
    {
        Debug.Log("FORMACION CAMBIADA");
        List<Character> lista = VarEstaticas.personajes.ToList();
        lista.RemoveAll(x => x == null); //Quita todos los nulls
        string nombres = "";
        foreach (var item in lista)
        {
            nombres += item.nombre;
        }
        Debug.Log(nombres);
        int numEnVanguardia = VarEstaticas.numEnVanguardia;
        var datos = DatosController.Singleton;
        datos.currentSave.formacion = new FormacionEquipo<Character>(lista, numEnVanguardia);
        aceptado();
        //HAY QUE CLONARLO Y GUARDARLO EN DATOS
        MenuController.formacion = panelActivo;
    }

    public void aceptado()
    {
        aceptar.gameObject.SetActive(false);
        check.gameObject.SetActive(true);
        VarEstaticas.personajes = new Character[5];
    }
}

//TODO PASAR A SINGLETON
public static class VarEstaticas
{
    public static int numEnVanguardia = 0;
    public static int formationSize = 1;

    public static Character seleccion;
    public static Character[] personajes = new Character[5];
}