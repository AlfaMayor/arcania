﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public static MenuController singleton;
  
    Canvas canvas;
    public Button[] botones = new Button[9];
    public GameObject[] paneles = new GameObject[9];
    GameObject panelActivo = null;
    public List<Character> characters = new List<Character>();
    public Color orange, blue;
    public static GameObject formacion=null;
    public AudioClip[] sounds;
    public AudioSource reproductor;

    void Awake()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
        }
    }
  

    private void Start()
    {
        Debug.Log("STARTEANDO");
        canvas = GetComponent<Canvas>();
        if (canvas.enabled) canvas.enabled = false;
    }

    public void ToggleMenu()
    {
        //ActivarBotones(true);
        canvas.enabled = !canvas.enabled;
        ClosePanel();
    }

    public void MostrarPanel(int pos)
    {
        ClosePanel();
        botones[pos].GetComponent<Image>().color = orange;
        reproductor.PlayOneShot(sounds[0]);
        panelActivo = Instantiate(paneles[pos], transform);
        //ActivarBotones(false);
    }

    private void ClosePanel()
    {
        Destroy(panelActivo);
        for (int i = 0; i < botones.Length; i++)
        {
            botones[i].GetComponent<Image>().color = blue;
        }
    }
    /*ACTIVAR PARA PODER USAR CORRECTAMENTE EL MANDO
    public void ActivarBotones(bool estado)
    {
        foreach (var boton in botones)
        {
            boton.enabled = estado;
        }
    }*/
}

