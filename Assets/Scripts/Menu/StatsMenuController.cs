﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsMenuController : MonoBehaviour
{
    private MenuController menu;
    public CharBtnController[] btnChar;
    public GameObject info;
    public GenStCharController pCharacter;
    public Text[] stats;
    public Color orange, normal;

    private void Awake()
    {
        menu = MenuController.singleton;

        for (int i = 0; i < btnChar.Length; i++)
        {
            btnChar[i].assignCharacter(menu.characters[i]);
        }
    }

    public void showPanel(int pos)
    {
        changeColor();
        btnChar[pos].GetComponent<Image>().color = orange;
        info.SetActive(true);
        Character c = menu.characters[pos];
        StatBonus extra = c.GetExtraStats();
        pCharacter.assignCharacter(c);
        stats[0].text = c.atk +"+("+ extra.atk+")";
        stats[1].text = c.def +"+("+ extra.def+")";
        stats[2].text = c.magicAtk + "+(" + extra.mAt + ")";
        stats[3].text = c.magicDef + "+(" + extra.mDf + ")";
        stats[4].text = c.agility + "+(" + extra.agl + ")";
        stats[5].text = c.evasion + "+(" + extra.eva + ")";
        stats[6].text = c.accuracy + "+(" + extra.acc + ")";
        stats[7].text = c.luck + "+(" + extra.lck + ")";
    }

    private void changeColor()
    {
        for (int i = 0; i < btnChar.Length; i++)
        {
            btnChar[i].GetComponent<Image>().color = normal;
        }
    }
}
