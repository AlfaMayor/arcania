﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralMenuController : MonoBehaviour
{
    private MenuController menu;
    public GenStCharController[] pCharacter;
    // Start is called before the first frame update
    private void Awake()
    {
        menu = MenuController.singleton;
    }

    void Start()
    {
        for (int i = 0; i < pCharacter.Length; i++)
        {
            pCharacter[i].assignCharacter(menu.characters[i]);
        }
    }
}
