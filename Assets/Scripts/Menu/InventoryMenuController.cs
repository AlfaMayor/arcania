﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMenuController : MonoBehaviour
{

    private PlayerControlState Entrada = new PlayerControlState();

    private bool siguienteAccion;

    public int elementoMenuSeleccionado = 0;

    public PanelItem prefabBotonItem;

    public Transform contenedorItems;

    public GameObject panelExtra;

    public GameObject botoneraEquipables;

    private Inventory inventario;

    List<PanelItem> elementosInstanciados = new List<PanelItem>();


    // Start is called before the first frame update
    void Start()
    {
        inventario = DatosController.Singleton.currentSave.inventario;
        SeleccionarFiltro(0);
    }

    public void SeleccionarFiltro(int i)
    {
        if (i < 4)
        {
            MostrarBotoneraEquipables(false);
        }
        else
        {
            MostrarBotoneraEquipables(true);
        }

        switch (i)
        {
            case 0: //Todo
                {
                    MostrarItems<Item>();
                    break;
                }
            case 1: //Consumibles
                {
                    MostrarItems<Consumable>();
                    break;
                }
            case 2: //Exploración
                {
                    MostrarItems<Exploration>();
                    break;
                }
            case 3: //Clave
                {
                    MostrarItems<KeyItem>();
                    break;
                }
            case 4: //Abre filtro de equipo
                {
                    MostrarItems<Equipment>();
                    break;
                }
            case 5: //Collares
                {
                    MostrarItems<Necklace>();
                    break;
                }
            case 6: //Armas
                {
                    MostrarItems<Weapon>();
                    break;
                }
            case 7: //Anillos
                {
                    MostrarItems<Ring>();
                    break;
                }
            case 8: //Cascos
                {
                    MostrarItems<Helmet>();
                    break;
                }
            case 9: //Armaduras
                {
                    MostrarItems<Armor>();
                    break;
                }
            case 10: //Pantalones
                {
                    MostrarItems<Legs>();
                    break;
                }
            case 11: //Botas
                {
                    MostrarItems<Boots>();
                    break;
                }
            case 12: //Abrigos
                {
                    MostrarItems<Coat>();
                    break;
                }
            case 13: //Escudos
                {
                    MostrarItems<Shield>();
                    break;
                }
            case 14: //Luces
                {
                    MostrarItems<Light>();
                    break;
                }
            default:break;
        }


    }

    private void MostrarBotoneraEquipables(bool activo)
    {
        botoneraEquipables.SetActive(activo);
    }

    private void MostrarItems<T>() where T : Item
    {
        Debug.Log("Mostrando items de tipo " + typeof(T).Name);
        List<T> items = inventario.GetItemsOfType<T>();
        for (int i = elementosInstanciados.Count-1; i >= 0; i--)
        {
            var temp = elementosInstanciados[i];
            elementosInstanciados.RemoveAt(i);
            Destroy(temp.gameObject);

        }

        foreach (var item in items)
        {
            var nuevaInstancia = Instantiate(prefabBotonItem, contenedorItems);
            nuevaInstancia.SetItem(item);
            elementosInstanciados.Add(nuevaInstancia);
        }

    }

}

