﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerInput
{
    /// <summary>
    /// Guarda la entrada de teclado/mando en el objeto pasado por parámetro
    /// </summary>
    /// <param name="output"></param>
    public static void GetInput(PlayerControlState output)
    {
        output.XAxis = Input.GetAxis("Horizontal");
        output.YAxis = Input.GetAxis("Vertical");
        output.Running = Input.GetButton("Run");
        output.RunDown = Input.GetButtonDown("Run");
        output.MenuDown = Input.GetButtonDown("Menu");
        output.Action = Input.GetButtonDown("Use");
    }

    /// <summary>
    /// Devuelve todos los valores de entrada a 0
    /// </summary>
    /// <param name="output"></param>
    public static void ResetInput(PlayerControlState output)
    {
        output.XAxis = 0;
        output.YAxis = 0;
        output.Running = false;
        output.RunDown = false;
        output.MenuDown = false;
        output.Action = false;
    }



}

/// <summary>
/// La entrada de control para el jugador, ya sea teclado o mando
/// </summary>
[Serializable]
public class PlayerControlState
{

    [SerializeField] private Vector2 curDir = Vector2.zero;
    [SerializeField] private Vector2 lastDir = Vector2.down;

    [SerializeField] private bool running;
    [SerializeField] private bool runDown;
    [SerializeField] private bool menuToggle;
    [SerializeField] private bool menuDown;
    [SerializeField] private bool action;
    public Vector2 CurDir { get => curDir; }
    public Vector2 LastDir { get => lastDir; }
    public float XAxis
    {
        get => curDir.x;
        set
        {
            float temp = Math.Abs(value) > VariablesEntorno.DEAD_ZONE ? value : 0;
            if (temp != 0)
            {
                lastDir.x = CurDir.x;
                lastDir.y = curDir.y;
            }
            curDir.x = temp;
        }
    }
    public float YAxis
    {
        get => curDir.y;
        set
        {
            float temp = Math.Abs(value) > VariablesEntorno.DEAD_ZONE ? value : 0;
            if (temp != 0)
            {
                lastDir.x = CurDir.x;
                lastDir.y = curDir.y;
            }
            curDir.y = temp;
        }
    }
    public bool Running { get => running; set => running = value; }
    public bool RunDown { get => runDown; set => runDown = value; }
    public bool MenuToggle
    {
        get => menuToggle; private set
        {
            //Al escribirlo así nos aseguramos que al pulsar el botón de menú podemos volvernos a mover tarde o temprano
            if (value) //TODO Quizas esta forma de manejar el menú dará problemas
            {
                menuToggle = !menuToggle;
            }
        }
    }
    public bool MenuDown
    {
        get => menuDown; set
        {
            if (value) MenuToggle = true;
            menuDown = value;
        }
    }
    public bool Action { get => action; set => action = value; }

}
