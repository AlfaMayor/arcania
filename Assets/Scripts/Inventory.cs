﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory
{
    public List<Item> todosLosItems = new List<Item>();

    public int dinero = 0;

    public Inventory()
    {

    }

    public Inventory(List<Item> items, int coins)
    {
        todosLosItems = items;
        dinero = coins;
    }


    /// <summary>
    /// Devuelve el objeto con el id proporcionado y del tipo pedido
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="id"></param>
    /// <returns></returns>
    public T GetItem<T>(string id) where T : Item
    {
        List<T> temp = GetItemsOfType<T>();

        foreach (var item in temp)
        {
            if (item.id.Equals(id)) return item;
        }

        return null;
    }

    public List<T> GetItemsOfType<T>() where T : Item
    {
        List<T> temp = new List<T>();

        foreach (var item in todosLosItems)
        {
            if (item is T) temp.Add(item as T);
        }


        return temp;
    }

    public void AddItem(Item item)
    {
        Item temp = CheckIfExists(item);
        if (temp == null)
        {
            todosLosItems.Add(item);
        }
        else
        {
            temp.cantidad += item.cantidad;
        }
    }

    public void RemoveItem(string id)
    {
        GetItem<Item>(id);
    }

    public void RemoveItem(Item item)
    {
        todosLosItems.Remove(item);
    }

    public Item CheckIfExists(Item i)
    {
        foreach (var item in todosLosItems)
        {
            if (item.id.Equals(i.id)) return item;
        }
        return null;
    }
}