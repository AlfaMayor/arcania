﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour
{
    public PlayerControlState Entrada = new PlayerControlState();
    public AudioClip sound;
    public AudioSource reproductor;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        PlayerInput.GetInput(Entrada);
        
        if (Entrada.Action)
        {
            reproductor.PlayOneShot(sound);
            StartCoroutine(Wait(0.8f));
        }
    }

    public IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene("Intro");
    }
}
