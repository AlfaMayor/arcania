﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroGameController : MonoBehaviour
{
    public AudioClip[] sounds;
    public AudioSource reproductor;

    private delegate void Accion();
   
    public void NewGame()
    {
        reproductor.PlayOneShot(sounds[0]);
        StartCoroutine(WaitAndExecute(1f,NuevaPartida));
    }

    public void LoadGame()
    {
        reproductor.PlayOneShot(sounds[0]);
        StartCoroutine(WaitAndExecute(1f,CargarPartida));
    }

    public void Options()
    {
        reproductor.PlayOneShot(sounds[1]);
        StartCoroutine(WaitAndExecute(0.5f,Opciones));
    }

    private IEnumerator WaitAndExecute(float time, Accion accion)
    {
        yield return new WaitForSeconds(time);
        accion.Invoke();
    }

    private void NuevaPartida()
    {
        var save = SaveGameData.SaveToSlot();
        SaveGameData.LoadFromSlot(save.saveId);
        SceneManager.LoadScene("Overworld");
    }

    private void CargarPartida()
    {
        SceneManager.LoadScene("LoadGame");
    }

    private void Opciones()
    {
        SceneManager.LoadScene("PlayerPreferences");
    }
}
