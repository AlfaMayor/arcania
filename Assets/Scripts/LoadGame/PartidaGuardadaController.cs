﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartidaGuardadaController : MonoBehaviour
{
    PantallaCargaController controlador;

    SaveGameData save;

    public int id;
    public Text slotText,
        unlockedChars,
        avgLevel;
    public Button button;

    private void Start()
    {
        controlador = FindObjectOfType<PantallaCargaController>();

        slotText.text += id;
        save = SaveGameData.GetSaveFileFromSlot(id);

        if (save == null)
        {
            GetComponent<Image>().color = Color.grey;
            unlockedChars.text = "VACÍO";
            avgLevel.text = "";
            button.enabled = false;
        }
        else
        {
            int avgLvl = 0;
            foreach (var item in save.refGrupo)
            {
                avgLvl += item.lvl;
            }
            avgLvl /= save.refGrupo.Count;
            unlockedChars.text += save.refGrupo.Count;
            avgLevel.text += avgLvl;
        }
    }

    public void LoadGame()
    {
        if (!controlador.cambiandoDePantalla)
        {
            controlador.cambiandoDePantalla = true;
            //Termina de deserializar la partida guardada y la asigna como partida cargada
            DatosController.Singleton.currentSave = SaveGameData.LoadFromSave(save);

            controlador.PartidaCargada();
        }
    }

}
