﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PantallaCargaController : MonoBehaviour
{
    public PlayerControlState Entrada = new PlayerControlState();

    public bool cambiandoDePantalla = false;

    public PartidaGuardadaController[] pgc;
    public AudioClip[] sounds;
    public AudioSource reproductor;
    private delegate void Accion();
    
    // Update is called once per frame
    void Update()
    {
        PlayerInput.GetInput(Entrada);
        if (Entrada.RunDown) Atras();
    }
    public void Atras()
    {
        if (!cambiandoDePantalla)
        {
            cambiandoDePantalla = true;
            reproductor.PlayOneShot(sounds[0]);
            StartCoroutine(WaitAndExecute(1f, Back));
        }
    }
    private IEnumerator WaitAndExecute(float time, Accion accion)
    {
        yield return new WaitForSeconds(time);
        accion.Invoke();
    }
    private void Back()
    {
        SceneManager.LoadScene("Intro");
    }

    internal void PartidaCargada()
    {
        reproductor.PlayOneShot(sounds[1]);
        //Carga el Overworld
        StartCoroutine(WaitAndExecute(1f, () => SceneManager.LoadScene("Overworld")));
    }
}
