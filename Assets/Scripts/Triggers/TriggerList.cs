﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Este script guarda una lista de todos los objetos que heredan de BaseTrigger
/// </summary>
[ExecuteInEditMode]
public class TriggerList : MonoBehaviour
{

    public static TriggerList Singleton { get; private set; }

    private void Awake()
    {
        if (!Singleton)
        {
            Singleton = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        teleportTriggerList.Clear();
        eventTriggerList.Clear();
        combatAreaList.Clear();
        safeAreaList.Clear();
        var triggers = FindObjectsOfType<BaseTrigger>();
        foreach (var item in triggers)
        {
            SaveTrigger(item);
        }
    }

    private void Update()
    {
        QuitaNulls(teleportTriggerList);
        QuitaNulls(eventTriggerList);
        QuitaNulls(combatAreaList);
        QuitaNulls(safeAreaList);
    }

    private static void QuitaNulls<T>(List<T> lista) where T:BaseTrigger
    {
        lista.RemoveAll(item =>
        {
            bool flag = item == null;
            if (flag)
            {
                flag = true;
            }
            return item == null;
        });
    }




    public List<TeleportTrigger> teleportTriggerList = new List<TeleportTrigger>();

    public List<EventTrigger> eventTriggerList = new List<EventTrigger>();

    public List<WildAreaTrigger> combatAreaList = new List<WildAreaTrigger>();

    public List<SafeAreaTrigger> safeAreaList = new List<SafeAreaTrigger>();

    /// <summary>
    /// Guarda un trigger en la lista correspondiente
    /// </summary>
    /// <param name="trigger">El trigger a guardar</param>
    public void SaveTrigger(BaseTrigger trigger)
    {
        Type type = trigger.GetType();
        if (type == typeof(TeleportTrigger))
        {
            teleportTriggerList.Add((TeleportTrigger)trigger);
        }
        else if (type == typeof(EventTrigger))
        {
            eventTriggerList.Add((EventTrigger)trigger);
        }
        else if (type == typeof(WildAreaTrigger))
        {
            combatAreaList.Add((WildAreaTrigger)trigger);
        }
        else if (type == typeof(SafeAreaTrigger))
        {
            safeAreaList.Add((SafeAreaTrigger)trigger);
        }
    }

    /// <summary>
    /// Borra un Trigger de la lista que le corresponde. Tiene que ser un objeto que hereda de BaseTrigger
    /// </summary>
    /// <param name="trigger">El Trigger a borrar</param>
    public void DeleteTrigger(BaseTrigger trigger)
    {
        Type type = trigger.GetType();
        if (type == typeof(TeleportTrigger))
        {
            teleportTriggerList.Remove((TeleportTrigger)trigger);
        }
        else if (type == typeof(EventTrigger))
        {
            eventTriggerList.Remove((EventTrigger)trigger);
        }
        else if (type == typeof(WildAreaTrigger))
        {
            combatAreaList.Remove((WildAreaTrigger)trigger);
        }
        else if (type == typeof(SafeAreaTrigger))
        {
            safeAreaList.Remove((SafeAreaTrigger)trigger);
        }
    }


}