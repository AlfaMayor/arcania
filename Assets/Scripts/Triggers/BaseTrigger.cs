﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase base para triggers de eventos
/// </summary>
[RequireComponent(typeof(Collider2D))]
public abstract class BaseTrigger : MonoBehaviour
{


    //Evento a lanzar
    public GameEventWParams eventoParametrizado;

    protected abstract void OnTriggerEnter2D(Collider2D collision);

    protected abstract void OnTriggerExit2D(Collider2D collision);

}
