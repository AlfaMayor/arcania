﻿using UnityEngine;

public class WildAreaTrigger : BaseTrigger
{

    public int area = 0;

    //Tanto este como SafeAreaTrigger tienen prácticamente el mismo código. Son clases separadas sólo por cuestión de diferenciarlos al colocar prefabs

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        //Primero comprueba que el evento no esté activo en ese momento
        if (!eventoParametrizado.isRaised)
        {
            //Si el evento no està activo, comprueba si el objeto que ha colisionado es un jugador
            if (collision.gameObject.GetComponent<Player>())
            {
                //Si lo es activa el evento
                eventoParametrizado.Raise(null);
                DatosController.Singleton.currentSave.currentArea = area;
            }
        }
    }

    protected override void OnTriggerExit2D(Collider2D collision)
    {
        //Primero comprueba que el evento esté activo en ese momento
        if (eventoParametrizado.isRaised)
        {
            //Si el evento está activo, comprueba si el objeto que ha colisionado es un jugador
            if (collision.gameObject.GetComponent<Player>())
            {
                //Si lo es "desactiva" el evento
                eventoParametrizado.Lower();
            }
        }
    }
}
