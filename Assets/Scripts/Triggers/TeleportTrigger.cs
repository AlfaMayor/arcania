﻿using UnityEngine;

public class TeleportTrigger : BaseTrigger
{

    public Transform target;

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        //Primero comprueba que el evento no esté activo en ese momento
        if (!eventoParametrizado.isRaised)
        {
            //Si el evento no està activo, comprueba si el objeto que ha colisionado es un jugador
            if (collision.gameObject.GetComponent<Player>())
            {

                //Si no existe un objetivo ni siquiera lo intentes y envia un mensaje de error en su lugar
                if (target) 
                {
                    EventParameters parametros = new EventParameters() { paramVector2 = target.position }; //Coge la posición del objetivo al que está vinculado este teletransportador

                    eventoParametrizado.Raise(parametros, true); //Ya ha comprobado que el evento no está activo, así que puede saltarse la segunda comprobación
                    eventoParametrizado.Lower();
                }
                else
                {
                    Debug.LogError("EL TELETRANSPORTADOR NO TIENE DESTINO");
                }
            }
        }
    }

    protected override void OnTriggerExit2D(Collider2D collision) { }  //No hace falta que haga nada

}
