﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PreferenciasController : MonoBehaviour
{

    private static string volumenMusica = "sliderVolMusic"; public static float VolumenMusica { get => PlayerPrefs.GetFloat(volumenMusica, 0.5f); set => PlayerPrefs.SetFloat(volumenMusica, value); }
    private static string volumenSFX = "sliderVolSFX"; public static float VolumenSFX { get => PlayerPrefs.GetFloat(volumenSFX, 0.5f); set => PlayerPrefs.SetFloat(volumenSFX, value); }

    public Slider sliderVolumenMusica;
    public Slider sliderVolumenSFX;

    private void Start()
    {
        LoadPrefs();
    }

    private void LoadPrefs()
    {
        sliderVolumenMusica.value = VolumenMusica;
        sliderVolumenSFX.value = VolumenSFX;
    }

    public void SavePrefs()
    {
        VolumenMusica = sliderVolumenMusica.value;
        VolumenSFX = sliderVolumenSFX.value;
    }

    public void GoBack()
    {
        SceneManager.LoadScene("Intro");
    }

}