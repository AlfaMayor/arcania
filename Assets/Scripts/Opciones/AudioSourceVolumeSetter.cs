﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceVolumeSetter : MonoBehaviour
{
    public MusicOrSFX type;

    public bool testing;

    private AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        if (source)
        {
            switch (type)
            {
                case MusicOrSFX.MUSIC:
                    source.volume = PreferenciasController.VolumenMusica;
                    break;
                case MusicOrSFX.SFX:
                    source.volume = PreferenciasController.VolumenSFX;
                    break;
                default:
                    break;
            }

            //En la ventana de opciones podemos probar el volumen, para eso está esta opción
            if (testing) InvokeRepeating("UpdateVolume", 0.2f, 0.2f);
            else Destroy(this); //Si no estamos probando el audio este componente ya no es necesario
        }
        else Destroy(this); //Si no estamos probando el audio este componente ya no es necesario

    }

    private void UpdateVolume()
    {
        switch (type)
        {
            case MusicOrSFX.MUSIC:
                source.volume = PreferenciasController.VolumenMusica;
                break;
            case MusicOrSFX.SFX:
                source.volume = PreferenciasController.VolumenSFX;
                break;
            default:
                break;
        }
    }

}

public enum MusicOrSFX
{
    MUSIC, SFX
}
