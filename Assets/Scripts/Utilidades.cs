﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Utilidades
{
    //Esta clase se usa para funciones varias que se pueden llamar desde cualquier parte del proyecto
#if UNITY_EDITOR
    /// <summary>
    /// Coge todos los ScriptableObjects del tipo especificado que haya en el juego
    /// </summary>
    /// <typeparam name="T">El tipo de ScriptableObject</typeparam>
    /// <returns>Un array con los objetos encontrados</returns>
    public static T[] GetAllInstances<T>() where T : ScriptableObject
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<T>(path);
        }

        return a;

    }

    /// <summary>
    /// Coge el primer ScriptableObject del tipo especificado que haya en el juego
    /// </summary>
    /// <typeparam name="T">El tipo de ScriptableObject</typeparam>
    /// <returns>El primer objeto encontrado (o null si no hay)</returns>
    public static T GetInstance<T>() where T : ScriptableObject
    {
        string guid = AssetDatabase.FindAssets("t:" + typeof(T).Name)[0];  //FindAssets uses tags check documentation for more info
        
        string path = AssetDatabase.GUIDToAssetPath(guid);

        T a = AssetDatabase.LoadAssetAtPath<T>(path);

        return a;

    }
#endif
    public static void SwapSlots<T>(List<T> lista, T personaje1, T personaje2) where T:Stats
    {
        int pos1 = lista.IndexOf(personaje1);
        int pos2 = lista.IndexOf(personaje2);
        lista[pos1] = personaje2;
        lista[pos2] = personaje1;
    }
}
