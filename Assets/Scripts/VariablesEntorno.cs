﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>Constantes usadas en múltiples scripts</summary>
public static class VariablesEntorno
{
    #region Constantes

    //Escala de movimiento (afecta a todos los valores relacionados al movimiento por igual)
    public static float MOVE_SCALE { get => 100f; }

    //Zona muerta de movimiento. También aplica a teclado ya que Unity emula un mando virtual para usar el teclado
    public static float DEAD_ZONE { get => 0.05f; }

    #endregion


}
