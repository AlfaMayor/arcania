﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventosAtrasados : MonoBehaviour
{

    //Esta clase activa todos los eventos alzados desde otra escena al cargar la escena en la que se encuentra

    public List<GameEvent> listaEventos = new List<GameEvent>();

    public List<GameEventWParams> listaEventos2 = new List<GameEventWParams>();

    void Start()
    {
        foreach (var evento in listaEventos)
        {
            if (evento.delayed)
            {
                evento.Raise().Lower();
                evento.delayed = false;
            }
        }
        foreach (var evento in listaEventos2)
        {
            evento.Raise().Lower();
        }
    }

}
