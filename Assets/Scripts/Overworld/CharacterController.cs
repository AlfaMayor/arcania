﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterController : MonoBehaviour
{

    #region Constantes

    //Parámetros de movimiento para el personaje
    public PlayerOverwoldStats StatsMovimiento = new PlayerOverwoldStats();

    public float runningAnimSpeed;
    public float walkingAnimSpeed;

    //IDs de parámetros de animación
    protected int animXAxis = Animator.StringToHash("XAxis");
    protected int animYAxis = Animator.StringToHash("YAxis");
    protected int animWalkSpeed = Animator.StringToHash("MovementSpeed");
    protected int animSpeed = Animator.StringToHash("AnimationSpeed");
    protected int animXLook = Animator.StringToHash("LastX");
    protected int animYLook = Animator.StringToHash("LastY");

    #endregion

    #region Variables

    public PlayerControlState Entrada = new PlayerControlState();
    protected Rigidbody2D rb;
    protected Animator animator;
    protected Vector2 direccion = new Vector2(0, -1);

    #endregion


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        SetUp();
    }

    /// <summary>
    /// Extensión de Start para los requerimientos específicos de cada objeto que herede de esta clase
    /// </summary>
    protected abstract void SetUp();

    protected virtual void Update()
    {
        GetEntrada();
        Animar();
    }

    protected virtual void FixedUpdate()
    {
        Movimiento();
    }

    /// <summary>
    /// La entrada de control del personaje, cada tipo (NPC o jugador) tiene una función distinta aquí
    /// </summary>
    protected abstract void GetEntrada();

    /// <summary>
    /// Movimiento del personaje en base a la entrada recibida. Por lo general todos los personajes se mueven igual, pero pueden haber excepciones
    /// </summary>
    protected virtual void Movimiento()
    {
        Vector2 movimiento = Entrada.CurDir.normalized;
        if (Math.Abs(movimiento.x) > Mathf.Epsilon || Math.Abs(movimiento.y) > Mathf.Epsilon)
        {
            movimiento *= Entrada.Running ? StatsMovimiento.RunSpeed : StatsMovimiento.WalkSpeed; //Dependiendo de si corre o no asigna la velocidad adecuada

            movimiento *= VariablesEntorno.MOVE_SCALE * Time.fixedDeltaTime;

            rb.velocity = movimiento;

            direccion = movimiento.normalized;
        }
    }

    /// <summary>
    /// Todos los personajes tienen el mismo AnimationController, asi que tienen las mismas claves a usar. Aún así el método es virtual por si hay alguna excepción a la regla
    /// </summary>
    protected virtual void Animar()
    {
        //Assigna valor actual de moviment horitzontal (si no és massa petit)
        float temp = Math.Abs(Entrada.XAxis) > 0.05f ? Entrada.XAxis : 0;
        animator.SetFloat(animXAxis, Entrada.XAxis);

        temp = Math.Abs(Entrada.YAxis) > 0.05f ? Entrada.YAxis : 0;
        animator.SetFloat(animYAxis, Entrada.YAxis);

        animator.SetFloat(animWalkSpeed, Entrada.CurDir.magnitude);

        //animator.SetBool(animRun, Entrada.Running);

        animator.SetFloat(animSpeed, Entrada.Running ? runningAnimSpeed : walkingAnimSpeed);

        animator.SetFloat(animXLook, Entrada.LastDir.x);
        animator.SetFloat(animYLook, Entrada.LastDir.y);

    }

    #region Respuesta a eventos

    /// <summary>
    /// Gira el personaje para que mire en una dirección determinada.
    /// </summary>
    /// <param name="eventParameters">Contenedor de parámetros. Ésta función usa paramVector2 para determinar la dirección</param>
    public void FaceDirection(EventParameters eventParameters)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Gira el personaje hacia un objeto en el mapa
    /// </summary>
    /// <param name="eventParameters">Contenedor de parámetros. Ésta función usa paramVector2 para determinar las coordenadas del objetivo</param>
    public void LookAt(EventParameters eventParameters)
    {
        throw new NotImplementedException();
    }

    #endregion


}

#region Clases auxiliares

/// <summary>
/// Define las características de movimiento del jugador
/// </summary>
[Serializable]
public class PlayerOverwoldStats
{
    public float WalkSpeed;
    public float RunSpeed;
}

#endregion
