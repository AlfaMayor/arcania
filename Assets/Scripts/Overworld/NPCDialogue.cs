﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDialogue : Interactable
{

    /*
    Optimización posible: Cada NPC guarda sus propios diálogos en variables, también son quienes controlan el canvas de diálogos.
    Esta optimización no se ha hecho a causa de que nos hemos dado cuenta demasiado tarde (29/5/2020)
        */

    CanvasDialogoController canvasDialogo;

    //ID del NPC
    public int npcId;

    public int currentDialogue;

    //Diálogos que puede ofrecer al jugador
    public List<int> dialogueIds;

    private int dialogueLine = 0;

    private void Start()
    {
        canvasDialogo = FindObjectOfType<CanvasDialogoController>();
    }

    public bool TriggerDialogue()
    {
        //Siguiente línea, si ya ha llegado a la última, cierra ventana de diálogo
        bool flag = canvasDialogo.GetLineaDialogo(dialogueIds[currentDialogue], dialogueLine);

        if (flag)
        {
            if (currentDialogue + 1 < dialogueIds.Count) currentDialogue++;
            dialogueLine = 0;
        }
        else
        {
            dialogueLine++;
        }

        return !flag; //Devuelve true mientras estemos en medio de la conversación

    }

    public override void Interact(Player jugador)
    {
        //Siguiente línea, si ya ha llegado a la última, cierra ventana de diálogo
        bool flag = canvasDialogo.GetLineaDialogo(dialogueIds[currentDialogue], dialogueLine);

        if (flag)
        {
            if (currentDialogue + 1 < dialogueIds.Count) currentDialogue++;
            dialogueLine = 0;
        }
        else
        {
            dialogueLine++;
        }

        jugador.inDialogue = !flag;
    }



    /* //Código original (por si es necesario volver)
    if (numDialogo < canvasDialogo.dialogos.Count)
    {
        inDialogue = true;
        canvasDialogo.c.enabled = true;
        canvasDialogo.changeCharacter(numDialogo);
        numDialogo++;
    }
    else
    {
        canvasDialogo.c.enabled = false;
        numDialogo = 0;
        inDialogue = false;
    }
    */



}
