﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

/// <summary>
/// Controlador del jugador cuando estás en el mapa
/// </summary>
public class Player : CharacterController
{

    #region Constantes

    private const float IMMUNITY_TIME = 3f;
    private const float COMBAT_TIMER = 5f;
    public GameEventWParams EVENTO_MENU;

    #endregion

    #region Variables

    public bool menuOpened = false;
    public bool inDialogue = false;
    public bool immunity = true;
    public Coroutine combate;

    #endregion

    protected override void SetUp()
    {
        var pos = DatosController.Singleton;
        if (pos != null)
        {
            if (pos.overworldIniciado)
                MoveTo(pos.currentSave.playerPos);
            else pos.overworldIniciado = true;
        }

    }

    protected override void GetEntrada()
    {
        PlayerInput.GetInput(Entrada);


        if (Entrada.Action)
        {
            Vector2 position = new Vector2(transform.position.x, transform.position.y);
            StartCoroutine(ExtDebug.DrawBox(position + (direccion * 1.5f), 0.5f, Quaternion.identity, Color.red, 1f));
            var hit = Physics2D.OverlapBox((direccion * 1.5f) + position, new Vector2(0.5f, 0.5f), 0f);
            Interactable interactable;
            if (hit)
            {
                if (interactable = hit.GetComponent<Interactable>())
                {
                    interactable.Interact(this);
                }
            }
        }

        if (Entrada.MenuDown)
        {
            EVENTO_MENU.Raise(null); //No necesita parámetros
            EVENTO_MENU.Lower();
        }
    }

    protected override void Update()
    {
        GetEntrada();
        if (!Entrada.MenuToggle && !inDialogue)
            Animar();
    }

    protected override void FixedUpdate()
    {

        if (!Entrada.MenuToggle && !inDialogue)
        {
            Movimiento();
        }

    }


    #region Manejo de zonas de combate

    public void EmpezarConteo()
    {
        if (combate == null)
        {
            combate = StartCoroutine(GetFight());
        }

    }

    public void PararConteo()
    {
        if (combate != null)
        {
            StopCoroutine(combate);
            combate = null;
        }
    }

    public IEnumerator GetFight()
    {
        float t = 0;
        float tCombate = Random.Range(IMMUNITY_TIME, IMMUNITY_TIME + COMBAT_TIMER);
        while (t <= tCombate)
        {
            if (Entrada.XAxis > 0 || Entrada.YAxis > 0)
            {
                t += Time.deltaTime;
            }
            yield return null;
        }
        combate = null;
        var datos = DatosController.Singleton;
        datos.currentSave.playerPos = transform.position;
        GetRandomEnemyTeam(datos);
        SceneManager.LoadScene("Combate");
    }

    private void GetRandomEnemyTeam(DatosController datos)
    {
        datos.equipoEnemigo = MonsterList.Singleton.GetRandomMonsterTeam(datos.currentArea);
    }

    #endregion

    public void Teleport(EventParameters eventParam)
    {
        MoveTo(eventParam.paramVector2);
    }

    public void MoveTo(Vector2 pos)
    {
        Vector3 newPos = new Vector3(pos.x, pos.y, transform.position.z); //Mantiene al personaje en la misma posición Z
        transform.position = newPos;
    }

}
