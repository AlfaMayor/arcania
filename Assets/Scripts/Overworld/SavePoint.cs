﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : Interactable
{

    CanvasDialogoController canvasDialogo;

    public Animator animator;

    //ID del NPC
    public int npcId;

    //Diálogo de guardado
    public int dialogueId;

    private int dialogueLine = 0;

    private void Start()
    {
        canvasDialogo = FindObjectOfType<CanvasDialogoController>();
    }

    public override void Interact(Player jugador)
    {


        //Siguiente línea, si ya ha llegado a la última, cierra ventana de diálogo
        bool flag = canvasDialogo.GetLineaDialogo(dialogueId, dialogueLine);

        if (flag)
        {
            var save = DatosController.Singleton.currentSave;

            save.playerPos = jugador.transform.position;

            SaveGameData.SaveToSlot(save);
            dialogueLine = 0;
        }
        else
        {
            dialogueLine++;
        }
            animator.SetBool("Save", !flag);

        jugador.inDialogue = !flag;


    }

}
