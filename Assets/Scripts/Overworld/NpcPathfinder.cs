﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using UnityEngine.Tilemaps;

public class NpcPathfinder : CharacterController
{

    public Transform target;

    public float nextWaypointDistance = 0f;

    Path path;
    int currentWaypoint = 0;

    public bool showGizmos = false;
    public Vector3 currentWaypointCoords;

    Seeker seeker;

    // SetUp es llamado al terminar Start
    protected override void SetUp()
    {
        seeker = GetComponent<Seeker>();

        seeker.StartPath(rb.position, target.position, OnPathCalculated); //Prueba, envia al NPC hacia donde se encuentra el jugador al cargar la escena

    }

    protected override void GetEntrada()
    {
        //Si no hay ruta a seguir vuelve inmediatamente
        if (path == null) return;

        //Si no hay siguiente punto en la ruta elimina la ruta, pone la velocidad a 0 y vuelve inmediatamente
        if (currentWaypoint >= path.vectorPath.Count)
        {
            path = null;
            rb.velocity = Vector2.zero;
            Entrada.XAxis = 0f;
            Entrada.YAxis = 0f;
            return;
        }

        //Obtiene la dirección en la que tiene que ir
        Vector2 dir = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;

        //La guarda para el script de movimiento
        Entrada.XAxis = dir.x;
        Entrada.YAxis = dir.y;


        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;

            //DEBUG Muestra con una caja roja el lugar marcado como objetivo actual
            if (showGizmos && currentWaypoint < path.vectorPath.Count)
            {
                currentWaypointCoords = path.vectorPath[currentWaypoint];
                StartCoroutine(ExtDebug.DrawBox(currentWaypointCoords, 0.5f, Quaternion.identity, Color.red, 0.5f));
            }
        }



    }

    /// <summary>
    /// Función llamada cuando se termina de montar una ruta para el NPC
    /// </summary>
    /// <param name="p"></param>
    void OnPathCalculated(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    #region Respuesta a eventos

    /// <summary>
    /// Traza una ruta para el personaje hasta el punto designado en los parámetros.
    /// </summary>
    /// <param name="eventParameters">Contenedor de parámetros (esta función usa paramVector2 para determinar las coordenadas objetivo)</param>
    public void MoveTo(EventParameters eventParameters)
    {
        //Coge las coordenadas de destino
        Vector3 targetPos = new Vector3(eventParameters.paramVector2.x, eventParameters.paramVector2.y, transform.position.z);
        Tilemap tm = FindObjectOfType<Tilemap>();

        //Tanto los tilemaps como el grid de pathfinding usan celdas del mismo tamaño, asi que uso uno de los tilemaps existentes para calcular la celda destino
        var targetCell = tm.CellToWorld(tm.WorldToCell(targetPos));

        seeker.StartPath(rb.position, targetCell, OnPathCalculated);
    }

    #endregion


}
