﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebasPartidaGuardada : MonoBehaviour
{

    public Inventory inventarioPlantilla;
    public int formacion;
    public List<Character> grupo;
    public Vector2 pos;

    void Awake()
    {
        if (DatosController.Singleton.currentSave == null)
            Save();
        Destroy(gameObject);
    }

    public void Save()
    {
        SaveGameData save = SaveGameData.CreateSaveObject(true);

        save.newGame = false;
        save.personajesEquipo = grupo;
        save.formacion = new FormacionEquipo<Character>(grupo, formacion);
        save.playerPos = pos;
        save.inventario = inventarioPlantilla;

        var newSave = SaveGameData.SaveToSlot(save);
        int id = newSave.saveId;
        newSave = SaveGameData.LoadFromSlot(id); //Aseguramos que todos los objetos que vamos a usar NO son los originales

        var datos = DatosController.Singleton;
        if (datos.currentSave == null)
        {
            datos.currentSave = newSave;
        }
        datos.inventario = newSave.inventario;
    }

    public void Load(int id)
    {
        var save = SaveGameData.LoadFromSlot(id);
        var datos = DatosController.Singleton;
        datos.currentSave = save;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
