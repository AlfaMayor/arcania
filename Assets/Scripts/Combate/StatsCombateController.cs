﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsCombateController : MonoBehaviour
{
    public Image face;
    public Text nombre, hp, mp, ep;
    public Character c;

    public void UpdateData()
    {
        if (c == null)
        {
            Debug.LogError("Se ha intentado actualizar la información de un panel no asignado a datos.");
        }
        face.sprite = c.sprite;
        nombre.text = c.nombre;
        hp.text = "HP: " + c.hp + "/" + c.hpMax;
        mp.text = "MP: " + c.mp + "/" + c.mpMax;
        ep.text = "EP: " + c.energy + "/" + c.maxEnergy;
    }

    public void SetCharacter(Character c)
    {
        this.c = c;
        UpdateData();
        SetVisibility(true);
    }

    public void SetVisibility(bool active = false)
    {
        var images = GetComponentsInChildren<Image>();
        var text = GetComponentsInChildren<Text>();

        foreach (var item in images)
        {
            item.enabled = active;
        }
        foreach (var item in text)
        {
            item.enabled = active;
        }
    }
}
