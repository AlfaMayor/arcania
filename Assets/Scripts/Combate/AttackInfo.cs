﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AttackInfo<T> where T : Stats
{
    public int target;
    public T attacker;
    public Skill skill;

}
