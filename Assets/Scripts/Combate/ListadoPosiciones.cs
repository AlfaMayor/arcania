﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListadoPosiciones : MonoBehaviour
{
    public int grupo = 0;

    public PosicionCombate[] vanguardia = new PosicionCombate[5];
    public PosicionCombate[] retaguardia = new PosicionCombate[5];

    /// <summary>
    /// Devuelve la lista de posiciones que tiene guardada
    /// </summary>
    /// <returns>Un array bidimensional con la base de posiciones</returns>
    public PosicionCombate[][] GetArray()
    {
        return new PosicionCombate[][] { vanguardia, retaguardia };
    }


}
