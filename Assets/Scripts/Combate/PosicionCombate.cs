﻿using System;
using UnityEngine;


public class PosicionCombate : MonoBehaviour
{

    public Stats personaje;

    public Renderer basePosicion;

    public Animator elementoAnimator;
    public Animator movimientoAnimator;
    public Animator animator;
    public TextMesh text;

    public Material selectedColor;

    private Material baseColor;

    public Color neutralColor, healColor, dmgColor, critDmgColor;

    private int attackTriggerID = Animator.StringToHash("Attack");
    private int hitTriggerID = Animator.StringToHash("Hit");
    private int specialTriggerID = Animator.StringToHash("Special");
    private int stateFloatID = Animator.StringToHash("State");
    private int[] triggerAnimElemental = new int[] {
        Animator.StringToHash("Aqua"),
        Animator.StringToHash("Flam"),
        Animator.StringToHash("Frigo"),
        Animator.StringToHash("Vis"),
        Animator.StringToHash("Vita"),
        Animator.StringToHash("Mort"),
        Animator.StringToHash("Physic")
    };

    private void Start()
    {
        baseColor = basePosicion.material;
    }

    public void Highlight(bool flag)
    {
        basePosicion.material = flag ? selectedColor : baseColor;
    }

    public void SetAnimator(RuntimeAnimatorController animatorController)
    {
        if (animatorController != null)
            animator.runtimeAnimatorController = animatorController;
    }

    /// <summary>
    /// Animación de uso de objeto
    /// </summary>
    /// <param name="tipo">El objeto tenia principalmente efectos beneficiosos o perjudiciales, o ambos por igual</param>
    /// <param name="texto">Resultado de usar el objeto (datos a mostrar)</param>
    public void ItemUse(BuffOrDebuff tipo, string texto)
    {
        ShowChanges(tipo, texto);
        TriggerElement(Element.VITA);
    }

    public void UpdateIdle(float hpPercent)
    {
        movimientoAnimator.SetFloat(stateFloatID, hpPercent); //Por si hacemos que el personaje se mueva un poco al estar con salud alta, debilitado o derrotado
        animator.SetFloat(stateFloatID, hpPercent);
    }
    public void BooleanAnimation(int id, bool flag)
    {
        movimientoAnimator.SetBool(id, flag);
        animator.SetBool(id, flag);
    }

    public void TriggerAnimation(int id)
    {
        movimientoAnimator.SetTrigger(id);
        animator.SetTrigger(id);
    }


    /// <summary>
    /// Muestra el daño hecho al personaje por pantalla con una pequeña animación. Calcula si es daño o curación
    /// </summary>
    /// <param name="damage">El valor numérico a mostrar</param>
    public void ShowDamage(int damage, string extra = "", bool critical = false)
    {
        text.GetComponent<Animator>().SetTrigger(attackTriggerID);
        text.text = damage + extra;

        BuffOrDebuff hod;

        //Cambia el color del texto segun si ha curado o dañado al personaje
        if (damage > 0) hod = critical ? BuffOrDebuff.CritDamage : BuffOrDebuff.Damage;
        else if (damage < 0) hod = BuffOrDebuff.Heal;
        else hod = critical ? BuffOrDebuff.CritDamage : BuffOrDebuff.Neutral;

        TriggerAnimation(hitTriggerID);
        ShowChanges(hod, damage + extra);

    }

    /// <summary>
    /// Muestra la información de cambios en el personaje encima de éste con una pequeña animación
    /// </summary>
    /// <param name="tipo">Daño, curación o neutro (inmunidad, no hay cambios, etc.)</param>
    /// <param name="texto">Texto a mostrar</param>
    public void ShowChanges(BuffOrDebuff tipo, string texto)
    {
        text.GetComponent<Animator>().SetTrigger(attackTriggerID);
        text.text = texto;

        switch (tipo)
        {
            case BuffOrDebuff.Heal:
                text.color = healColor;
                break;
            case BuffOrDebuff.Neutral:
                text.color = neutralColor;
                break;
            case BuffOrDebuff.Damage:
                text.color = dmgColor;
                break;
            case BuffOrDebuff.CritDamage:
                text.color = critDmgColor;
                break;
            default:
                break;
        }
    }

    public void TriggerElement(Element element)
    {
        TriggerElement((int)element);
    }

    public void TriggerElement(int id)
    {
        elementoAnimator.SetTrigger(triggerAnimElemental[id]);
    }
    public void Esconder()
    {
        gameObject.SetActive(false);
    }

    internal void Mostrar()
    {
        gameObject.SetActive(true);
    }
}
