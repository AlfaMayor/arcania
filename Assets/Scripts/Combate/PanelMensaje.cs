﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelMensaje : MonoBehaviour
{
    public Image imagen;
    public Text texto;

    public void ShowMessage(Sprite sprite, string message, float duration = 3f)
    {
        imagen.sprite=sprite;
        texto.text = message;


        gameObject.SetActive(true);

        Invoke("Hide", duration);
    }


    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
