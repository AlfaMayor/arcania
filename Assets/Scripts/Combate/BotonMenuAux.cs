﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BotonMenuAux : MonoBehaviour
{
    public Stats character;

    public Text nombre;
    public Text detalles;
    public int posicion = 0;

    public GameEventWParams eventoOnClick;

    private Button button;

    private void Start()
    {
        button = GetComponent<Button>();
    }

    public void SetData(string strNombre, string strExtra, int pos)
    {
        if (nombre != null)
            nombre.text = strNombre;
        if (detalles != null)
            detalles.text = strExtra;
        posicion = pos;
    }

    public void OnClick()
    {
        EventParameters ep = new EventParameters() { paramInt = posicion };
        eventoOnClick.Raise(ep).Lower();
    }

    public void SetActive(bool active)
    {
        if (character != null && button != null)
        {
            if (character.hp <= 0) button.enabled = false;
            else button.enabled = active;
        }
    }

}
