﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuAuxiliar : MonoBehaviour
{
    public GameObject contenedorLista;
    public BotonMenuAux prefab;

    public List<BotonMenuAux> botonesInstanciados;

    public void SetData(List<string> column1, List<string> column2)
    {
        ResetData();
        for (int i = 0; i < column1.Count && i < column2.Count; i++)
        {
            AddData(column1[i], column2[i], i);
        }
        SetActiveButtons(true);
    }

    public void SetData(List<PosicionCombate> objetivos)
    {
        ResetData();
        int k = 0;
        foreach (var objetivo in objetivos)
        {
            var boton = AddData(objetivo.personaje.nombre, "", k);
            boton.character = objetivo.personaje;
            if (objetivo.personaje.hp <= 0) boton.GetComponent<Button>().enabled = false;
            k++;
        }
        SetActiveButtons(true);
    }

    public void SetData(List<string> column1)
    {
        ResetData();
        for (int i = 0; i < column1.Count; i++)
        {
            AddData(column1[i], "", i);
        }
        SetActiveButtons(true);
    }

    public BotonMenuAux AddData(string column1, string column2, int i)
    {
        BotonMenuAux boton = Instantiate(prefab, contenedorLista.transform);
        boton.SetData(column1, column2, i);
        botonesInstanciados.Add(boton);
        return boton;
    }

    public void ResetData()
    {
        botonesInstanciados.ForEach(x => Destroy(x.gameObject));
        botonesInstanciados.Clear();
    }

    /// <summary>
    /// Como el SetActive de GameObject, pero específico para los botones de esta lista. También marca como seleccionado el primer botón de la lista
    /// </summary>
    /// <param name="active">Si los botones tienen que estar activos o no</param>
    public void SetActiveButtons(bool active)
    {
        if (botonesInstanciados.Count > 0)
        {
            foreach (var button in botonesInstanciados)
            {
                button.SetActive(active);
            }
            if (active)
                botonesInstanciados[0].GetComponent<Button>().Select(); //Marca el primer botón de la lista como seleccionado
        }
    }

    internal void SetVisible(bool flag)
    {
        gameObject.SetActive(flag);
        SetActiveButtons(flag);
    }
}
