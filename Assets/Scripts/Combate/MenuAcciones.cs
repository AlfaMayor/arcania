﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuAcciones : MonoBehaviour
{
    public List<Button> botones;

    /// <summary>
    /// Como el SetActive de GameObject, pero específico para los botones de esta lista. También marca como seleccionado el primer botón de la lista
    /// </summary>
    /// <param name="active">Si los botones tienen que estar activos o no</param>
    public void SetActiveButtons(bool active)
    {
        if (botones.Count > 0)
        {
            foreach (var button in botones)
            {
                button.enabled = active;
            }
            if (active)
                botones[0].Select(); //Marca el primer botón de la lista como seleccionado
        }
    }
}
