﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CombatSceneController : MonoBehaviour
{
    //Facilita llamar animaciones de personajes específicos desde otros scripts
    private static CombatSceneController singleton;

    public static CombatSceneController Singleton
    {
        get
        {
            if (!singleton) singleton = FindObjectOfType<CombatSceneController>();
            return singleton;
        }
    }

    #region Constantes

    private const int BASE_HIT_CHANCE = 50;
    private const int BASE_MISS_CHANCE = 20; //Probabilidades de fallar el golpe sin tener en cuenta estadísticas de los personajes
    private const int BASE_NON_CRIT_CHANCE = 150;
    private const float DMG_ABSORPTION_RATE = 0.25f; //Cuanta reducción de ataque por defensor que tenga el objetivo
    private const float DMG_ABSORPTION_LIMIT = 0.85f; //Máximo de daño que pueden absorber los defensores del objetivo
    private const float CRIT_MULTIPLIER = 2f;

    [NonSerialized] public int triggerAttack = Animator.StringToHash("Attack");
    [NonSerialized] public int triggerSpecial = Animator.StringToHash("Special");
    [NonSerialized] public int triggerEvade = Animator.StringToHash("Evasion");
    [NonSerialized] public int animVictoriaBool = Animator.StringToHash("Victory");


    #endregion

    #region variables

    /// <summary>Panel usado para el menú principal del combate (acciones principales)</summary>
    public MenuAcciones panelAcciones;
    /// <summary>Panel usado para los menús de habilidades, objetos, etc.</summary>
    public MenuAuxiliar panelAuxiliar;
    /// <summary>Panel usado para seleccionar el objetivo de la acción</summary>
    public MenuAuxiliar panelObjetivos;
    /// <summary>Ventana de mensajes. Usada principalmente para mostrar el nombre del ataque usado.</summary>
    public PanelMensaje panelMensaje;

    //Plantillas prefabricadas de las diferentes formaciones posibles
    public GameObject[] formaciones;
    public Button[] bFormaciones;
    private GameObject plantillaSeleccionada;

    /// <summary>Lista de las imágenes mostradas en el borde izquierdo de la pantalla.</summary>
    public Image[] turnos;

    /// <summary>Contiene la lista de objetos que muestran el estado actual de nuestros aliados.</summary>
    public List<StatsCombateController> statsAliados;

    /// <summary>Lista de controladores de las representaciones visuales de tus aliados que hay en el campo de batalla.</summary>
    public List<PosicionCombate> representacionVisualAliados;
    /// <summary>Lista de controladores de las representaciones visuales de los enemigos que hay en el campo de batalla.</summary>
    public List<PosicionCombate> representacionVisualEnemigos;
    /// <summary>Lista de controladores de las representaciones visuales de todos los personajes que hay en el campo de batalla.</summary>
    public List<PosicionCombate> representacionVisualGeneral;

    /// <summary>Lista de personajes que estan actualmente en la lista de objetivos (panelObjetivos)</summary>
    public List<PosicionCombate> personajesEnElPuntoDeMira = new List<PosicionCombate>();


    /// <summary>Variables para la música de combate</summary>
    public AudioClip[] songs;
    public AudioSource reproductor;

    #region Variables turno jugador

    /// <summary>Por ahora no se usa, pero si deshechamos la selección por botones y usamos las representaciones visuales directamente para seleccionar un objetivo, esto será necesario</summary>
    PlayerControlState Entrada = new PlayerControlState();

    PosicionCombate objetivoSeleccionado;

    /// <summary>Usado para saber qué boton del menú se ha elegido</summary>
    int elementoMenuSeleccionado = 0;
    /// <summary>Usado para saber en qué parte del comando elegido (ataque, skill, item, etc.) nos encontramos</summary>
    [SerializeField] int progresoMenu = 0;

    //Indica que la corutina puede hacer el siguiente ciclo del bucle de turno
    bool siguienteAccionTurno = false;

    [SerializeField]
    EstadoTurno estado;

    #endregion



    [SerializeField]
    private List<Stats> ordenTurnos = new List<Stats>();

    public FormacionEnemiga enemigos;
    public FormacionAliada aliados;

    public GameEventWParams eventoZonaCombate;


    #endregion

    #region Pre-Combate

    private void Start()
    {
        panelObjetivos.SetVisible(false);
        panelAuxiliar.SetVisible(false);
        panelMensaje.gameObject.SetActive(false);

        var datos = DatosController.Singleton;


        enemigos = new FormacionEnemiga(datos.equipoEnemigo);
        aliados = new FormacionAliada(datos.currentSave.formacion);

        //Añade todos los personajes a una misma lista y los ordena por agilidad,
        //quedando primero el que tenga el valor más alto
        ordenTurnos.AddRange(aliados.lista);
        ordenTurnos.AddRange(enemigos.lista);
        ordenTurnos.Sort();

        //Resproduce una canción aleatoria de las definidas para esta escena
        int cancion = Random.Range(0, songs.Length);
        reproductor.PlayOneShot(songs[cancion]);

        //Asigna posiciones a los personajes y en el caso de aliados la ventana de información (margen inferior derecho)
        ColocarTodo();



        //Empezamos el bucle de combate
        StartCoroutine(BucleCombate());

    }

    private void ColocarTodo()
    {

        //Asigna los personajes a cada espacio y hace invisibles los demás
        for (int i = statsAliados.Count - 1; i >= 0; i--)
        {
            if (i < aliados.lista.Count)
            {
                statsAliados[i].SetCharacter(aliados.lista[i]); //Asigna el personaje al panel de información
            }
            else
            {
                statsAliados[i].SetVisibility();
                statsAliados.RemoveAt(i); //No hay más aliados, así que no hace falta actualizarlo cada turno
            }
        }

        ListadoPosiciones[] tempPosiciones = FindObjectsOfType<ListadoPosiciones>();

        representacionVisualEnemigos.Clear();
        representacionVisualAliados.Clear();

        //Preparamos los grupos
        foreach (var item in tempPosiciones)
        {
            if (item.grupo == 0) //El grupo de aliados es el 0, el de enemigos el 1
                PrepararFormacion(aliados, representacionVisualAliados, item);
            else
                PrepararFormacion(enemigos, representacionVisualEnemigos, item);
        }

        //Juntamos los personajes de ambos grupos en uno sólo (para ataques que pueden afectar a cualquiera)
        representacionVisualGeneral = new List<PosicionCombate>();
        representacionVisualGeneral.AddRange(representacionVisualAliados);
        representacionVisualGeneral.AddRange(representacionVisualEnemigos);
    }

    #region Colocación formaciones
    private void PrepararFormacion<T>(FormacionCombate<T> grupo, List<PosicionCombate> representacionVisual, ListadoPosiciones listadoPosiciones) where T : Stats
    {
        List<T> lista = grupo.lista;
        int numEnVanguardia = grupo.numEnVanguardia;
        T[][] formacion = new T[][] { new T[5], new T[5] };

        if (numEnVanguardia == 0 || numEnVanguardia > lista.Count) numEnVanguardia = lista.Count;
        if (numEnVanguardia > 3) numEnVanguardia = 3;

        PosicionCombate[][] arrayPosicionesvisuales = listadoPosiciones.GetArray();

        //Coloca a los personajes en su lugar correspondiente
        ColocarGrupo(lista, formacion, numEnVanguardia, representacionVisual, arrayPosicionesvisuales);

        //Facilita las cosas a la hora de calcular la absorción de daño al atacar la retaguardia
        grupo.formacionEnCombate = formacion;

        //Guarda la lista de GameObjects que representan a cada uno de los personajes de la formación
        grupo.representacionVisual = representacionVisual;

        EsconderPosicionesSobrantes(representacionVisual, arrayPosicionesvisuales);

    }

    private void ColocarGrupo<T>(List<T> lista, T[][] formacion, int numEnVanguardia, List<PosicionCombate> posiciones, PosicionCombate[][] arrayPosicionesVisuales) where T : Stats
    {
        bool lineaPar = numEnVanguardia % 2 == 0; //Número de personajes par en la vanguardia
        bool modificandoRetaguardia = false;
        int k = 0;

        for (int i = 0; i < formacion.Length; i++)
        {
            for (int j = 2; j >= 0; j--)
            {
                //Primero pasa por vanguardia y luego por retaguardia
                if (modificandoRetaguardia)
                {
                    if (k >= lista.Count) break; //Ha llegado al límite de personajes en el grupo
                }
                else
                {
                    if (k >= numEnVanguardia) break; //Ha llegado al límite de personajes en la vanguardia
                }

                if (lineaPar)
                {
                    if (j % 2 != 0) //Coloca sólo en posiciones pares. No es necesario comprobar casilla central
                    {
                        ColocarPersonaje(lista[k], formacion, i, j, posiciones, arrayPosicionesVisuales);
                        k++;
                        ColocarPersonaje(lista[k], formacion, i, formacion[i].Length - 1 - j, posiciones, arrayPosicionesVisuales);
                        k++;
                    }
                }
                else
                {
                    if (j % 2 == 0) //Coloca sólo en posiciones impares
                    {
                        ColocarPersonaje(lista[k], formacion, i, j, posiciones, arrayPosicionesVisuales);
                        k++;
                        if (j != 2) //Si no está en la posición central coloca un personaje en el lado contrario
                        {
                            ColocarPersonaje(lista[k], formacion, i, formacion[i].Length - 1 - j, posiciones, arrayPosicionesVisuales);
                            k++;
                        }
                    }
                }
            }
            lineaPar = (lista.Count - numEnVanguardia) % 2 == 0; //El unico grupo en el que ambas líneas pueden ser par es en la cuadrilla
            modificandoRetaguardia = true;
        }
    }

    private void ColocarPersonaje<T>(T personaje, T[][] formacion, int x, int y, List<PosicionCombate> posiciones, PosicionCombate[][] arrayPosicionesVisuales) where T : Stats
    {
        if (formacion[x][y] != personaje)
        {
            formacion[x][y] = personaje; posiciones.Add(arrayPosicionesVisuales[x][y]);
            arrayPosicionesVisuales[x][y].SetAnimator(personaje.animatorCombate);
            arrayPosicionesVisuales[x][y].personaje = personaje;
        }
    }

    private void EsconderPosicionesSobrantes(List<PosicionCombate> lista, PosicionCombate[][] array)
    {
        foreach (var fila in array)
        {
            foreach (var item in fila)
            {
                if (!lista.Contains(item)) item.Esconder();
                else item.Mostrar();
            }
        }
    }

    #endregion

    #endregion

    #region Bucle de combate
    private IEnumerator BucleCombate()
    {
        //Ésta función hace uso de corutinas anidadas (espera hasta que termina la corutina ejecutada)

        bool inCombat = true;

        while (inCombat)
        {
            for (int i = 0; i < ordenTurnos.Count; i++)
            {
                UpdateTurnos(i); //Actualiza la lista de turnos (mostrada en el lado izquierdo de la pantalla)

                if (ordenTurnos[i] is Character jugador && jugador.hp > 0) yield return StartCoroutine(TurnoJugador(jugador));
                else if (ordenTurnos[i] is Monster monster && monster.hp > 0) yield return StartCoroutine(TurnoEnemigo(monster));

                //Cambia animaciones de "idle" según el porcentaje de salud de cada personaje
                //Y muestra la información actualizada de los aliados
                ActualizarInfoPantalla();

                if (ordenTurnos[i].hp > 0)
                    yield return new WaitForSecondsRealtime(1f); //Espera antes de empezar el siguiente turno

                inCombat = enemigos.SumaHp() > 0 && aliados.SumaHp() > 0;
                if (!inCombat) break; //Interrumpe el ciclo ya que no quedan acciones que hacer contra el rival
            }
        }

        //Termina el combate
        if (aliados.SumaHp() > 0)
        {
            yield return StartCoroutine(Victoria());
        }
        else if (enemigos.SumaHp() > 0)
        {
            yield return StartCoroutine(Derrota());
        }

    }

    /// <summary>Se llama al principio del turno, actualiza la lista de turnos (columna de imágenes representativas de cada personaje)</summary>
    /// <param name="pos">La posición del personaje cuyo turno ocurre a continuación</param>
    public void UpdateTurnos(int pos)
    {
        int i = pos, consecutivos = 0;

        foreach (var item in turnos)
        {
            while (ordenTurnos[i].hp <= 0 && consecutivos < 10)
            {
                consecutivos++;
                i++;
                if (i >= ordenTurnos.Count) i -= ordenTurnos.Count;
            }
            item.sprite = ordenTurnos[i].sprite;
            consecutivos = 0;
            i++;
            if (i >= ordenTurnos.Count) i -= ordenTurnos.Count;
        }
    }

    /// <summary>
    /// Se llama al final del turno, actualiza la información de tu equipo
    /// </summary>
    private void ActualizarInfoPantalla()
    {
        foreach (var item in statsAliados)
        {
            item.UpdateData();
        }

        for (int i = 0; i < representacionVisualAliados.Count && i < aliados.lista.Count; i++)
        {
            float hpPercent = (float)aliados.lista[i].hp / (float)aliados.lista[i].hpMax;
            representacionVisualAliados[i].UpdateIdle(hpPercent);
        }
        for (int i = 0; i < representacionVisualEnemigos.Count && i < enemigos.lista.Count; i++)
        {
            float hpPercent = (float)enemigos.lista[i].hp / (float)enemigos.lista[i].hpMax;
            representacionVisualEnemigos[i].UpdateIdle(hpPercent);
        }
    }

    #region Turno Enemigo

    private IEnumerator TurnoEnemigo(Monster monster)
    {
        
        yield return StartCoroutine(AIDecision(monster));
    }


    private IEnumerator AIDecision(Monster monster)
    {
        int eleccion = UnityEngine.Random.Range(0, 2);

        switch (eleccion)
        {
            case 0:
                yield return StartCoroutine(AIAttack(monster));
                break;
            case 1:
                yield return StartCoroutine(AISpecial(monster));
                break;
        }
        yield return null;
    }
    private IEnumerator AIAttack(Monster monster)
    {
        int t = 0;
        List<Stats> targets = GetTargets(Area.SINGLE, Target.ENEMIES).Select(X => X.personaje).ToList();
        do
        {
            t = UnityEngine.Random.Range(0, targets.Count);
            if (targets[t].hp > 0)
            {
                AttackInfo<Monster> atInfo = new AttackInfo<Monster>() { target = t, attacker = monster, skill = NormalAttack.Singleton };
                TriggerAnimation(monster, triggerAttack);
                RecibirAtaqueGeneral(aliados, atInfo);
            }
            yield return null;
        } while (targets[t].hp <= 0);
    }
    private IEnumerator AISpecial(Monster monster)
    {
        List<Skill> availableSkills = new List<Skill>();
        if (monster.skills != null && monster.skills.Count > 0)
        {
            foreach (var skill in monster.skills)
            {
                if ((skill.type.Equals(TypeOfSkill.SPELL) && skill.consumo >= monster.mp) || (skill.type.Equals(TypeOfSkill.ABILITY) && skill.consumo >= monster.energy))
                {
                    availableSkills.Add(skill);
                }
            }
            int r = UnityEngine.Random.Range(0, availableSkills.Count);
            Skill chosenSkill = availableSkills[r];

            if (chosenSkill.type.Equals(TypeOfSkill.SPELL))
            {
                if (chosenSkill.consumo <= monster.mp)
                {
                    int t = 0;
                    List<Stats> targets = GetTargets(chosenSkill.area, chosenSkill.objetivo).Select(X => X.personaje).ToList();
                    do
                    {
                        t = UnityEngine.Random.Range(0, targets.Count);
                        if (targets[t].hp > 0)
                        {
                            AttackInfo<Monster> atInfo = new AttackInfo<Monster>() { target = t, attacker = monster, skill = chosenSkill };
                            TriggerAnimation(monster, triggerSpecial);
                            RecibirAtaqueGeneral(enemigos, atInfo);
                        }
                        yield return null;
                    } while (targets[t].hp <= 0);
                }
                else
                {
                    yield return StartCoroutine(AIAttack(monster));
                }
            }
            else if (chosenSkill.type.Equals(TypeOfSkill.ABILITY))
            {
                if (chosenSkill.consumo <= monster.energy)
                {
                    int t = 0;
                    List<Stats> targets = GetTargets(chosenSkill.area, chosenSkill.objetivo).Select(x => x.personaje).ToList();
                    do
                    {
                        t = UnityEngine.Random.Range(0, targets.Count);
                        if (targets[t].hp > 0)
                        {
                            AttackInfo<Monster> atInfo = new AttackInfo<Monster>() { target = t, attacker = monster, skill = chosenSkill };
                            TriggerAnimation(monster, triggerSpecial);
                            RecibirAtaqueGeneral(enemigos, atInfo);
                        }
                        yield return null;
                    } while (targets[t].hp <= 0);
                }
                else
                {
                    yield return StartCoroutine(AIAttack(monster));
                }
            }
        }
        else
        {
            yield return StartCoroutine(AIAttack(monster));
        }
    }

    private List<Stats> AISelectTarget()
    {
        List<Stats> objetivos = new List<Stats>();

        foreach (var c in aliados.lista)
        {
            if (c.hp > 0)
            {
                objetivos.Add(c);
            }
        }
        return objetivos;
    }

    #endregion

    #region Turno jugador

    public bool VolverAlMenuAnterior()
    {
        PlayerInput.GetInput(Entrada);
        if (Entrada.RunDown)
        {
            Debug.Log("Acción cancelada");
            return true;
        }
        PlayerInput.ResetInput(Entrada);
        return false;
    }

    public void CambioEstado(int nuevoEstado) { estado = (EstadoTurno)nuevoEstado; }

    private IEnumerator TurnoJugador(Character personaje)
    {
        //Empieza el turno en espera, al pulsar uno de los botones del menú cambiará al estado adecuado
        estado = EstadoTurno.EN_ESPERA;
        siguienteAccionTurno = false;
        while (true)
        {
            switch (estado)
            {
                case EstadoTurno.EN_ESPERA:
                case EstadoTurno.MENU_PRINCIPAL:
                    //Habilita botones de la ventana de comandos (ataque, especial, item, posiciones, huir).
                    //Cierra ventanas extra (items, skills)
                    yield return StartCoroutine(MenuPrincipal(personaje));
                    break;
                case EstadoTurno.ATAQUE:
                    //Ataque básico, directamente pide elegir objetivo.
                    yield return StartCoroutine(AtaqueBasico(personaje));
                    break;
                case EstadoTurno.MENU_SKILLS:
                    //Abre el menu de skills, desactiva botones de ventana de comandos.
                    yield return StartCoroutine(MenuSkills(personaje));
                    break;
                case EstadoTurno.MENU_ITEMS:
                    //Abre el menu de items, desactiva botones de ventana de comandos.
                    yield return StartCoroutine(MenuItems(personaje));
                    break;
                case EstadoTurno.MENU_FORMACION:
                    //Abre el menú de formaciones, desactiva botones de ventana de comandos. 
                    //Muestra lista de equipo y pide seleccionar uno (el propio personaje sale desactivado en la lista)
                    yield return StartCoroutine(MenuFormaciones(personaje));
                    break;
                case EstadoTurno.HUYENDO:
                    //Comprueba si ha podido huir, si lo ha logrado vuelve al Overworld (funcion de salida),
                    //en caso contrario pierde turno
                    yield return StartCoroutine(IntentoDeHuida());
                    break;
                case EstadoTurno.TURNO_TERMINADO:
                    //Termina la corutina
                    yield break;
                default:
                    yield return null;
                    break;
            }
        }
    }

    private IEnumerator MenuPrincipal(Character personaje)
    {
        elementoMenuSeleccionado = 0;
        progresoMenu = 0;

        panelAcciones.SetActiveButtons(true);

        int pos = aliados.lista.IndexOf(personaje);
        if (pos >= aliados.numEnVanguardia)
        {
            if (personaje.weapon == null || !personaje.weapon.range) //Si el personaje no lleva un arma de largo alcance (un arco por ejemplo), no puede usar ataques básicos
                panelAcciones.botones[0].enabled = false;
        }
        if (personaje.skills.Count == 0) //Si el personaje no tiene habilidades impide el uso del botón
        {
            panelAcciones.botones[1].enabled = false;
        }

        while (estado == EstadoTurno.MENU_PRINCIPAL || estado == EstadoTurno.EN_ESPERA)
        {
            switch (progresoMenu)
            {
                case -1: //No hay otro menú antes de este
                    {
                        progresoMenu++;
                        break;
                    }
                case 0:
                    {
                        yield return StartCoroutine(WaitForMenu());

                        if (progresoMenu < 0) break;

                        switch (elementoMenuSeleccionado)
                        {
                            case 0: //Ataque
                                estado = EstadoTurno.ATAQUE;
                                break;
                            case 1: //Especial
                                estado = EstadoTurno.MENU_SKILLS;
                                break;
                            case 2: //Item
                                estado = EstadoTurno.MENU_ITEMS;
                                break;
                            case 3: //Formacion
                                estado = EstadoTurno.MENU_FORMACION;
                                break;
                            case 4: //Huir
                                estado = EstadoTurno.HUYENDO;
                                break;
                        }
                        break;
                    }
            }

        }
        panelAcciones.SetActiveButtons(false); //Desactiva el menú de comandos hasta que se vuelva a éste
    }

    private IEnumerator AtaqueBasico(Character personaje)
    {
        progresoMenu = 0;
        Skill skill = NormalAttack.Singleton;
        personajesEnElPuntoDeMira = GetTargets(skill.area, skill.objetivo);

        while (estado == EstadoTurno.ATAQUE)
        {
            switch (progresoMenu)
            {
                case -1: //Volver al menú principal
                    {
                        panelObjetivos.SetVisible(false);
                        estado = EstadoTurno.MENU_PRINCIPAL;
                        break;
                    }
                case 0: //Selección objetivo
                    {
                        panelObjetivos.SetData(personajesEnElPuntoDeMira);
                        panelObjetivos.SetVisible(true);

                        yield return StartCoroutine(WaitForMenu());

                        if (progresoMenu < 0 || elementoMenuSeleccionado < 0 || elementoMenuSeleccionado >= personajesEnElPuntoDeMira.Count) break;//Vuelve al menú anterior o valor incorrecto (lo cual no deberia pasar)

                        AttackInfo<Character> atInfo = new AttackInfo<Character>() { attacker = personaje, skill = skill, target = elementoMenuSeleccionado };

                        RecibirAtaqueGeneral(enemigos, atInfo); //A diferencia de menú skills, el ataque normal siempre ataca al enemigo y es de un solo objetivo, por lo cual no hace falta cambiarlo
                        progresoMenu++;
                        break;
                    }
                case 1: //Terminar turno
                    {
                        personajesEnElPuntoDeMira = null;
                        panelObjetivos.SetVisible(false);
                        estado = EstadoTurno.TURNO_TERMINADO;
                        break;
                    }
                default:
                    break;
            }
            yield return null;
        }


        yield return null;
    }

    private IEnumerator MenuSkills(Character personaje)
    {
        elementoMenuSeleccionado = 0;
        progresoMenu = 0;

        Skill skillElegido = null;


        personajesEnElPuntoDeMira = null;

        List<Skill> skills = personaje.skills;
        List<string> skillNames = new List<string>();
        List<string> skillCosts = new List<string>();
        foreach (var skill in skills)
        {
            skillNames.Add(skill.nombre);
            skillCosts.Add(skill.consumo + (skill.type == TypeOfSkill.SPELL ? " MP" : " EP"));
        }
        panelAuxiliar.SetData(skillNames, skillCosts);

        while (estado == EstadoTurno.MENU_SKILLS)
        {
            switch (progresoMenu)
            {
                case -1: //Volver al menú
                    {
                        panelAuxiliar.SetVisible(false);
                        estado = EstadoTurno.MENU_PRINCIPAL;
                        yield break;
                    }
                case 0: //Elige Skill
                    {
                        panelObjetivos.SetVisible(false);
                        panelAuxiliar.SetVisible(true);
                        yield return StartCoroutine(WaitForMenu());

                        if (progresoMenu < 0 || elementoMenuSeleccionado < 0 || elementoMenuSeleccionado >= skills.Count) break; //Vuelve al menú anterior o valor incorrecto (lo cual no deberia pasar)

                        if (skillElegido != skills[elementoMenuSeleccionado])
                        {
                            personajesEnElPuntoDeMira = null; //Si se ha vuelto desde el menú de selección de objetivo hay que recalcularlo
                            skillElegido = skills[elementoMenuSeleccionado];

                            if (skillElegido == null) continue; //Vuelve a pedir seleccionar habilidad (de nuevo, no deberia pasar)
                        }

                        if (SkillLanzable(personaje, skillElegido))
                            progresoMenu++; //Pasa a selección de objetivo si puede lanzar esta habilidad

                        break;
                    }
                case 1: //Elige objetivo
                    {
                        panelAuxiliar.SetActiveButtons(false);

                        if (skillElegido.area != Area.ALL)
                        {
                            if (personajesEnElPuntoDeMira == null) //Si ha cambiado el skill elegido vuelve a coger la lista de objetivos, en caso contrario no es necesario
                            {
                                personajesEnElPuntoDeMira = GetTargets(skillElegido.area, skillElegido.objetivo);
                            }

                            panelObjetivos.SetData(personajesEnElPuntoDeMira);
                            panelObjetivos.SetVisible(true);
                            panelAuxiliar.SetActiveButtons(false);

                            yield return StartCoroutine(WaitForMenu());

                            if (progresoMenu < 1 || elementoMenuSeleccionado < 0 || elementoMenuSeleccionado >= personajesEnElPuntoDeMira.Count) break; //Vuelve al menú anterior o valor incorrecto (lo cual no deberia pasar)

                            AttackInfo<Character> atInfo = new AttackInfo<Character>() { attacker = personaje, skill = skillElegido, target = elementoMenuSeleccionado };


                            //Como en los casos "ENEMIES" y "ALLIES" solo se tiene en cuenta un grupo, no hace falta recalcular cuál el el número correspondiente al personaje dentro del grupo
                            switch (skillElegido.objetivo)
                            {
                                case Target.ENEMIES:
                                    RecibirAtaqueGeneral(enemigos, atInfo);
                                    break;
                                case Target.ALLIES:
                                    RecibirAtaqueGeneral(aliados, atInfo);
                                    break;
                                case Target.ALL: //Toca conseguir el numero correcto (mezclamos las listas despues de todo)
                                    {
                                        Stats objetivo = personajesEnElPuntoDeMira[elementoMenuSeleccionado].personaje;
                                        elementoMenuSeleccionado = enemigos.lista.IndexOf(objetivo as Monster);
                                        atInfo.target = elementoMenuSeleccionado;
                                        if (elementoMenuSeleccionado >= 0) RecibirAtaqueGeneral(enemigos, atInfo);
                                        else
                                        {
                                            elementoMenuSeleccionado = aliados.lista.IndexOf(objetivo as Character);
                                            atInfo.target = elementoMenuSeleccionado;
                                            if (elementoMenuSeleccionado >= 0) RecibirAtaqueGeneral(aliados, atInfo);
                                            else
                                            {
                                                break; //Error, vuelve a pedir objetivo (no debería pasar)
                                            }
                                        }
                                    }
                                    break;
                            }
                            progresoMenu++;
                        }
                        else //Ataque en área, afecta a todos los objetivos
                        {
                            AttackInfo<Character> atInfo = new AttackInfo<Character>()
                            {
                                attacker = personaje,
                                skill = skillElegido,
                                target = 0 //No importa ya que ataca a todo el grupo
                            };

                            switch (skillElegido.objetivo)
                            {
                                case Target.ENEMIES:
                                    RecibirAtaqueGeneral(enemigos, atInfo);
                                    break;
                                case Target.ALLIES:
                                    RecibirAtaqueGeneral(aliados, atInfo);
                                    break;
                                case Target.ALL:
                                    RecibirAtaqueGeneral(enemigos, atInfo);
                                    RecibirAtaqueGeneral(aliados, atInfo);
                                    break;
                            }
                            progresoMenu++;

                        }

                        if (skillElegido.type == TypeOfSkill.SPELL) personaje.mp -= skillElegido.consumo;
                        else personaje.energy -= skillElegido.consumo;

                        break;
                    }
                case 2: //Finaliza turno
                    {
                        personajesEnElPuntoDeMira = null;
                        panelAuxiliar.SetVisible(false);
                        panelObjetivos.SetVisible(false);
                        estado = EstadoTurno.TURNO_TERMINADO;
                        break;
                    }
            }
            yield return null;
        }


    }

    private bool SkillLanzable(Character personaje, Skill skillElegido)
    {
        if (skillElegido.type == TypeOfSkill.SPELL)
        {
            return personaje.mp >= skillElegido.consumo;
        }
        else
        {
            return personaje.energy >= skillElegido.consumo;
        }
    }

    private IEnumerator MenuItems(Character personaje)
    {
        var datos = DatosController.Singleton;
        Inventory inventario = datos.currentSave.inventario;

        elementoMenuSeleccionado = 0;
        progresoMenu = 0;

        personajesEnElPuntoDeMira = null;
        List<string> nombresObjetivos = null;

        List<Consumable> items = inventario.GetItemsOfType<Consumable>();
        List<string> itemNames = new List<string>();
        List<string> itemAmounts = new List<string>();
        foreach (var item in items)
        {
            itemNames.Add(item.nombre);
            itemAmounts.Add(item.cantidad.ToString());
        }

        panelAuxiliar.SetData(itemNames, itemAmounts);

        Consumable itemElegido = null;

        while (estado == EstadoTurno.MENU_ITEMS)
        {

            switch (progresoMenu)
            {
                case -1: //Volver al menú
                    {
                        panelAuxiliar.SetVisible(false);
                        estado = EstadoTurno.MENU_PRINCIPAL;
                        yield break;
                    }
                case 0: //Elige item
                    {
                        panelObjetivos.SetVisible(false);
                        panelAuxiliar.SetVisible(true);
                        yield return StartCoroutine(WaitForMenu());

                        if (progresoMenu < 0 || elementoMenuSeleccionado < 0 || elementoMenuSeleccionado >= items.Count) continue; //Vuelve al menú anterior o valor incorrecto (lo cual no deberia pasar)

                        if (itemElegido != items[elementoMenuSeleccionado])
                        {
                            personajesEnElPuntoDeMira = null; //Si se ha vuelto desde el menú de selección de objetivo hay que recalcularlo
                            itemElegido = items[elementoMenuSeleccionado];

                            if (itemElegido == null) continue; //El objeto era null (de nuevo, no deberia pasar)
                        }

                        progresoMenu++; //Pasa a selección de objetivo si el objeto elegido no es un valor nulo

                        break;
                    }
                case 1: //Selección de objetivo
                    {
                        if (itemElegido.area != Area.ALL)
                        {
                            if (personajesEnElPuntoDeMira == null) //Si ha cambiado el skill elegido vuelve a coger la lista de objetivos, en caso contrario no es necesario
                            {
                                personajesEnElPuntoDeMira = GetTargets(itemElegido.area, itemElegido.objetivo);

                                nombresObjetivos = GetNames(personajesEnElPuntoDeMira);
                            }

                            panelObjetivos.SetData(nombresObjetivos);
                            panelObjetivos.SetVisible(true);
                            panelAuxiliar.SetActiveButtons(false);

                            yield return StartCoroutine(WaitForMenu());

                            if (progresoMenu < 1 || elementoMenuSeleccionado < 0 || elementoMenuSeleccionado >= personajesEnElPuntoDeMira.Count) continue; //Vuelve al menú anterior o valor incorrecto (lo cual no deberia pasar)

                            itemElegido.Consumir(personajesEnElPuntoDeMira[elementoMenuSeleccionado].personaje);

                        }
                        else //Afecta a todo el equipo, y si puede afectar a aliados y enemigos, afecta a ambos equipos
                        {
                            itemElegido.Consumir(personajesEnElPuntoDeMira.Select(x => { return x.personaje; }).ToList());
                        }
                        progresoMenu++;
                        break;
                    }
                case 2: //Final acción
                    {
                        personajesEnElPuntoDeMira.Clear();
                        panelAuxiliar.SetVisible(false);
                        panelObjetivos.SetVisible(false);
                        estado = EstadoTurno.TURNO_TERMINADO;
                        break;
                    }
                default:
                    break;
            }


            yield return null;
        }







    }
    private IEnumerator MenuFormaciones(Character personaje)
    {
        elementoMenuSeleccionado = 0;
        progresoMenu = 0;

        int tipoCambio = 0;

        List<string> opciones = new List<string>();
        opciones.Add("Cambiar posición");

        if (aliados.lista.IndexOf(personaje) == 0)
        {
            opciones.Add("Cambiar formación");
        }

        List<Character> objetivos = aliados.lista.Where(x => x != personaje).ToList(); //Todos los personajes que no sean el que está intentando cambiar posición
        personajesEnElPuntoDeMira = aliados.representacionVisual.Where(X => objetivos.Contains(X.personaje)).ToList();
        List<string> nombresObjetivos = GetNames(personajesEnElPuntoDeMira);

        nombresObjetivos = GetNames(personajesEnElPuntoDeMira);

        panelAuxiliar.SetData(opciones);

        while (estado == EstadoTurno.MENU_FORMACION)
        {
            switch (progresoMenu)
            {
                case -1: //Volver al menú
                    {
                        panelAuxiliar.SetVisible(false);
                        estado = EstadoTurno.MENU_PRINCIPAL;
                        yield break;
                    }
                case 0: //Seleccion tipo cambio
                    {
                        panelObjetivos.SetVisible(false);
                        panelAuxiliar.SetVisible(true);

                        yield return StartCoroutine(WaitForMenu());

                        tipoCambio = elementoMenuSeleccionado;
                        if (progresoMenu >= 0)
                            progresoMenu++;

                        break;
                    }
                case 1: //Seleccion Objetivo
                    {
                        panelAuxiliar.SetActiveButtons(false);
                        switch (tipoCambio)
                        {
                            case 0: //Cambia posición con otro personaje
                                {

                                    nombresObjetivos = GetNames(personajesEnElPuntoDeMira);

                                    panelObjetivos.SetData(nombresObjetivos);
                                    panelObjetivos.SetVisible(true);

                                    yield return StartCoroutine(WaitForMenu());

                                    if (progresoMenu < 1) break;

                                    Utilidades.SwapSlots(aliados.lista, personaje, objetivos[elementoMenuSeleccionado]);

                                    ColocarTodo(); //Recalcula formaciones

                                    progresoMenu++;

                                    break;
                                }
                            case 1: //Cambio completo de formación
                                {
                                    aliados.lista.Reverse();
                                    aliados.numEnVanguardia = aliados.lista.Count - aliados.numEnVanguardia;

                                    ColocarTodo();
                                    progresoMenu++;
                                    break;
                                }
                        }

                        break;
                    }
                case 2: //Finalizar Turno
                    {
                        personajesEnElPuntoDeMira.Clear();
                        panelAuxiliar.SetVisible(false);
                        panelObjetivos.SetVisible(false);
                        estado = EstadoTurno.TURNO_TERMINADO;
                        break;
                    }
            }
        }
    }


    #region Seleccion Menú/Objetivo

    /*
    private IEnumerator InputEspera(float t)
    {
        PlayerControlState temp = new PlayerControlState();
        while (true)
        {
            PlayerInput.GetInput(temp);
            if (temp.XAxis != 0 || temp.YAxis != 0)
            {
                Entrada = temp;
                yield return new WaitForSecondsRealtime(t);
            }
            yield return null;

        }
    }*/

    /// <summary>
    /// Espera a que se seleccione un botón del menú o el botón de cancelar/correr
    /// </summary>
    /// <returns>Corutina</returns>
    private IEnumerator WaitForMenu()
    {
        siguienteAccionTurno = false;
        while (!siguienteAccionTurno) //Espera a que se seleccione un objetivo
        {
            //Si pulsamos el botón de cancelar/correr volvemos al menú anterior

            PlayerInput.GetInput(Entrada);
            if (Entrada.RunDown)
            {
                progresoMenu--;
                siguienteAccionTurno = true;
                yield return null;
            }
            else
            {
                //Esto podria estar fuera del else si no fuera porque buscamos una variable que solo esta activa por un frame
                yield return null;
            }
        }
    }

    /// <summary>
    /// Enviado a través de evento (botones instanciados)
    /// </summary>
    /// <param name="ep">Parámetros de evento (en este caso usa paramInt)</param>
    public void SeleccionPorBoton(EventParameters ep)
    {
        SeleccionPorBoton(ep.paramInt);
    }

    /// <summary>
    /// Enviado a través de Button directamente (botones fijos en escena)
    /// </summary>
    /// <param name="i">Número asignado al botón</param>
    public void SeleccionPorBoton(int i)
    {
        elementoMenuSeleccionado = i;
        siguienteAccionTurno = true;
    }

    private IEnumerator SeleccionaConMandoOTeclado<T>(FormacionCombate<T> formacion) where T : Stats
    {
        objetivoSeleccionado = null;
        bool bucleFinalizado = false;
        int i = 0;
        do
        {
            /*
            //La intencion con este código era mover el cursor entre los personajes en pantalla
            if (Math.Abs(Entrada.XAxis)>Math.Abs(Entrada.YAxis)) //Cambia de línea
            {
                if (i < formacion.numEnVanguardia) i += formacion.numEnVanguardia;
                else i -= formacion.numEnVanguardia;
                if (i < 0) i = 0;
                else if (i >= formacion.lista.Count) i = formacion.lista.Count-1;
            }
            else
            {
                if (Entrada.YAxis < 0)
                {

                }
            }
            */

            if (Entrada.YAxis < 0)
            {
                i++;
            }
            else if (Entrada.YAxis > 0)
            {
                i--;
            }

            //Da la vuelta a la lista
            if (i < 0) i = formacion.lista.Count - 1;
            else if (i >= formacion.lista.Count) i = 0;

            bucleFinalizado = Entrada.Action && objetivoSeleccionado != null;
            if (Entrada.Running)
            {
                objetivoSeleccionado = null;
                bucleFinalizado = true;
            }
            PlayerInput.ResetInput(Entrada);
            yield return null;
        } while (!bucleFinalizado); //Continua el bucle mientras no se confirme o cancele la selección

    }

    private PosicionCombate SeleccionaConRaton(List<PosicionCombate> posiciones)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 50))
        {
            PosicionCombate pc = hit.transform.GetComponent<PosicionCombate>();
            if (pc != null)
            {
                if (posiciones.Contains(pc))
                {
                    return pc;
                }
            }
        }
        return null;
    }

    #endregion


    #endregion

    #endregion


    public void MostrarPlantillaFormacion(int num)
    {
        if (plantillaSeleccionada)
        {
            Destroy(plantillaSeleccionada);
        }
        plantillaSeleccionada = Instantiate(formaciones[num]);
    }


    #region Acciones

    #region Ataque

    private void RecibirAtaqueGeneral<T, X>(FormacionCombate<T> equipoObjetivo, AttackInfo<X> atInfo) where T : Stats where X : Stats
    {
        panelMensaje.ShowMessage(atInfo.attacker.sprite, atInfo.skill.nombre, 3f);

        TriggerAnimation(atInfo.attacker, triggerAttack);

        if (atInfo.skill.area == Area.ALL) //Ataque en área, se salta el sistema de bloqueo por formación y ataca a todos
        {
            for (int i = 0; i < equipoObjetivo.lista.Count; i++)
            {
                if (equipoObjetivo.lista[i].hp > 0) //Solo afecta a los que esten vivos
                {
                    AnimarEfectoElemental(equipoObjetivo.lista[i], atInfo.skill.elements);
                    atInfo.target = i;
                    RecibirAtaqueFinal(equipoObjetivo.lista[i], 1, atInfo);
                }
            }
        }
        else //Ataque a un sólo objetivo, cálculos de daño adicionales son necesarios
        {
            //Evita una excepción de tipo OutOfBoundsException
            if (atInfo.target >= equipoObjetivo.lista.Count || atInfo.target < 0)
            {
                print("El ataque de " + atInfo.attacker.nombre + " falló por mucho.");
                return;
            }


            //Comprueba si el objetivo evita el ataque
            T objetivo = equipoObjetivo.lista[atInfo.target];
            StatBonus sbObjetivo = objetivo.GetExtraStats();
            X atacante = atInfo.attacker;
            StatBonus sbAtacante = atacante.GetExtraStats();
            bool attackMisses = (atInfo.skill is NormalAttack) ? Random.Range(0, BASE_MISS_CHANCE + objetivo.evasion + sbObjetivo.eva) > Random.Range(0, BASE_HIT_CHANCE + atacante.accuracy + sbAtacante.acc) : false;

            AnimarEfectoElemental(objetivo, atInfo.skill.elements);

            if (attackMisses)
            {
                TriggerAnimation(objetivo, triggerEvade);
                Debug.Log("El ataque de " + atacante.nombre + " ha fallado");
                return;
            }
            else if (atInfo.target < equipoObjetivo.numEnVanguardia) //Ataca a vanguardia
            {
                RecibirAtaqueFinal(equipoObjetivo.lista[atInfo.target], 1, atInfo);
                return;
            }
            else //Ataca a retaguardia
            {
                RecibirAtaqueRetaguardia(equipoObjetivo, atInfo);
            }
        }

    }

    private void RecibirAtaqueRetaguardia<T, X>(FormacionCombate<T> formacion, AttackInfo<X> atInfo) where T : Stats where X : Stats
    {

        T[][] equipoObjetivo = formacion.formacionEnCombate;
        T objetivo = formacion.lista[atInfo.target];

        //Recibe el ataque como la vanguardia, pero parte del daño es redirigido a quienquiera que tenga delante dependiendo de la formación

        //El objetivo tiene que estar en la retaguardia

        if (objetivo == null)
        {
            Debug.LogError("Se ha puesto como objetivo un espacio vacío");
            return;
        }

        int pos = Array.IndexOf(equipoObjetivo[1], objetivo); //Obtiene la posición de la retaguardia en la que se encuentra el personaje

        //Llena una lista con los personajes que defienden al objetivo. Las magias no se pueden bloquear
        List<T> defensores = (atInfo.skill.type == TypeOfSkill.SPELL) ? new List<T>() : GetDefensores(equipoObjetivo, pos);

        //Porcentaje al cual es bajado la fuerza base del ataque del atacante. Número de 0 a DMG_ABSORPTION_LIMIT
        float ataqueBloqueado = DMG_ABSORPTION_RATE * defensores.Count;
        //Reduccion de daño limitada al 85%, por si acaso hacemos posible que sea mayor la abosrción de daño (más enemigos o aumentar el índice de absorción)
        if (ataqueBloqueado > DMG_ABSORPTION_LIMIT) ataqueBloqueado = DMG_ABSORPTION_LIMIT;


        //Los que estén delante de quien recibe el ataque absorben su parte del daño base (daño bloqueado / numero de defensores)
        foreach (T personaje in defensores)
        {
            RecibirAtaqueFinal(personaje, (ataqueBloqueado / defensores.Count), atInfo);
        }

        RecibirAtaqueFinal(objetivo, 1 - ataqueBloqueado, atInfo); //El objetivo recibe el daño mitigado por sus compañeros de la vanguardia

    }

    private void RecibirAtaqueFinal<T, X>(T objetivo, float multAtaqueBaseFinal, AttackInfo<X> atInfo) where T : Stats where X : Stats
    {
        X atacante = atInfo.attacker;
        StatBonus sbAtacante = atacante.GetExtraStats();
        Skill ataque = atInfo.skill;

        StatBonus sbObjetivo = objetivo.GetExtraStats();

        string mensajeConsola = "{0} ha recibido {1} de {2} de fuerza, que se ha visto reducido a {3} por su defensa y resistencia elemental.";

        float fuerzaBase = 0;
        float fuerzaMitigada = 0;

        float[] arrayAbsorcion = new float[7] { 1, 1, 1, 1, 1, 1, 1 };
        float[] arrayResistencia = new float[7] { 1, 1, 1, 1, 1, 1, 1 };

        //Encuentra los elementos coincidentes entre el ataque y el objetivo para determinar la resistencia elemental
        var coincidenciasElementales = objetivo.affinities.Where((afinidad) => { return ataque.elements.Contains(afinidad.element); });

        foreach (Affinities afinidad in coincidenciasElementales)
        {
            switch (afinidad.effect)
            {
                case AffinityType.ABSORB: //Quita el cálculo de daño por resistencia
                    {
                        //Asigna el en el espacio correspondiente en el array y anula la resistencia (sólo cura)
                        arrayAbsorcion[(int)afinidad.element] /= ((int)afinidad.multiplier / 2f);
                        arrayResistencia[(int)afinidad.element] = 0;
                        break;
                    }
                case AffinityType.RESIST:
                    {
                        //Asigna el valor en el espacio correspondiente en el array y anula la absorcion (sólo resistencia)
                        arrayResistencia[(int)afinidad.element] *= ((int)afinidad.multiplier / 2f);
                        arrayAbsorcion[(int)afinidad.element] = 0;
                        break;
                    }
            }
        }


        //Aplica ataque y defensa física o mágica
        switch (ataque.type)
        {
            //Si es un ataque físico tiene en cuenta las características físicas de ambos personajes
            case TypeOfSkill.ABILITY:
            case TypeOfSkill.NORMAL:
                {
                    fuerzaBase += atacante.atk;
                    fuerzaBase += sbAtacante.atk;

                    //El daño base se ve reducido a este porcentaje (depende de si ha sido defendido por aliados o si ha defendido a uno o ninguna de las dos cosas)
                    fuerzaBase *= multAtaqueBaseFinal;

                    fuerzaMitigada = fuerzaBase;

                    fuerzaMitigada -= objetivo.def;
                    fuerzaMitigada -= sbObjetivo.def;

                    break;
                }
            //Si es un ataque mágico tiene en cuenta las características elementales de ambos personajes
            case TypeOfSkill.SPELL:
                {
                    fuerzaBase += atacante.magicAtk;
                    fuerzaBase += sbAtacante.mAt;

                    if (ataque.multiplier > 0) //Ataque ofensivo
                    {
                        fuerzaBase *= multAtaqueBaseFinal; //El daño base se ve reducido a este porcentaje

                        fuerzaMitigada = fuerzaBase;

                        fuerzaMitigada -= objetivo.magicDef;
                        fuerzaMitigada -= sbObjetivo.mDf;
                    }
                    else
                    {
                        fuerzaBase *= multAtaqueBaseFinal;
                        fuerzaMitigada = fuerzaBase;
                    }
                    break;
                }
        }

        if (fuerzaMitigada < 0) fuerzaMitigada = 0;

        //Calcula si es golpe crítico (suerte)
        bool isCriticalHit = Random.Range(0, BASE_NON_CRIT_CHANCE + atacante.luck + sbAtacante.lck) < (atacante.luck + sbAtacante.lck);

        //Aplica el multiplicador del ataque y el de crítico si es un golpe crítico (este último calculado desde daño base)
        fuerzaMitigada *= ataque.multiplier + (fuerzaBase * (isCriticalHit ? CRIT_MULTIPLIER - 1 : 0));

        //Calcula la proporcion correspondiente a cada elemento
        float fuerzaDividida = fuerzaMitigada / ataque.elements.Length;

        float hpPerdido = 0;






        //Aplica las resistencias elementales (también curas por recibir ataques de ciertos elementos)
        foreach (Element elemento in ataque.elements)
        {
            float atkResistido = fuerzaDividida * arrayResistencia[(int)elemento];
            float atkAbsorbido = fuerzaDividida * arrayAbsorcion[(int)elemento] * ((ataque.multiplier > 0) ? 1 : -1); //La ternaria es para asegurarse que las curas no dañan al objetivo


            hpPerdido += atkResistido + atkAbsorbido;
        }







        objetivo.hp -= (int)hpPerdido;

        mensajeConsola = string.Format(mensajeConsola, objetivo.nombre, ataque.nombre, (int)fuerzaBase, (int)hpPerdido);

        if (objetivo.hp <= 0)
        {
            objetivo.hp = 0;
            mensajeConsola += " " + objetivo.nombre + " ha sido derrotado.";
        }
        else if (objetivo.hp > objetivo.hpMax) objetivo.hp = objetivo.hpMax;


        print(mensajeConsola);

        //Muestra el daño hecho al objetivo por pantalla (no es necesario casteo, pues ya tenemos su posicion en la formacion)
        //También activa la animación de recibir golpe para ese personaje
        TriggerDamage(objetivo, (int)hpPerdido, " HP", isCriticalHit);

    }

    #endregion

    #region Huida

    public IEnumerator IntentoDeHuida()
    {
        float valor = UnityEngine.Random.Range(0f, 1f);

        if (valor >= 0.3)  //No puedes huir si el número supera el porcentaje de huida (en este caso 30%)
        {
            print("No has podido huir.");
            estado = EstadoTurno.TURNO_TERMINADO;
            yield break;
        }

        print("Has logrado huir del combate");

        yield return null;
        Volver();
    }

    #endregion

    /// <summary>Volver al Overworld</summary>
    private void Volver()
    {
        eventoZonaCombate.Delay(null);
        SceneManager.LoadScene("Overworld");
    }

    #endregion


    #region Funciones Auxiliares
    //Roger: Estas funciones simplemente no sé en qué otra región deberían ir

    public void BooleanAnimation(Stats personaje, int idBoolean, bool flag)
    {
        int index;
        if (personaje is Character character && (index = aliados.lista.IndexOf(character)) >= 0)
        {
            representacionVisualAliados[index].BooleanAnimation(idBoolean, flag);
        }
        else if (personaje is Monster monster && (index = enemigos.lista.IndexOf(monster)) >= 0)
        {
            representacionVisualEnemigos[index].BooleanAnimation(idBoolean, flag);
        }
    }

    /// <summary>Activa una animación del personaje</summary>
    /// <param name="personaje">El personaje cuya animación va a ser activada</param>
    /// <param name="idTrigger">ID del activador de la animación</param>
    public void TriggerAnimation(Stats personaje, int idTrigger)
    {
        int index;
        if (personaje is Character character && (index = aliados.lista.IndexOf(character)) >= 0)
        {
            representacionVisualAliados[index].TriggerAnimation(idTrigger);
        }
        else if (personaje is Monster monster && (index = enemigos.lista.IndexOf(monster)) >= 0)
        {
            representacionVisualEnemigos[index].TriggerAnimation(idTrigger);
        }
    }

    public void TriggerDamage(Stats personaje, int damage, string extra, bool critical)
    {
        int index;
        if (personaje is Character character && (index = aliados.lista.IndexOf(character)) >= 0)
        {
            representacionVisualAliados[index].ShowDamage(damage, extra, critical);
        }
        else if (personaje is Monster monster && (index = enemigos.lista.IndexOf(monster)) >= 0)
        {
            representacionVisualEnemigos[index].ShowDamage(damage, extra, critical);
        }
    }

    /// <summary>
    /// Muestra cambios en el estado del personaje
    /// </summary>
    /// <param name="personaje">Personaje afectado</param>
    /// <param name="tipo">Efecto positivo, negativo o neutro</param>
    /// <param name="texto">Texto a mostrar</param>
    public void TriggerShowChanges(Stats personaje, BuffOrDebuff tipo, string texto)
    {
        int index;
        if (personaje is Character character && (index = aliados.lista.IndexOf(character)) >= 0)
        {
            representacionVisualAliados[index].ShowChanges(tipo, texto);
        }
        else if (personaje is Monster monster && (index = enemigos.lista.IndexOf(monster)) >= 0)
        {
            representacionVisualEnemigos[index].ShowChanges(tipo, texto);
        }
    }

    public void AnimarEfectoElemental(Stats objetivo, Element[] elementos)
    {
        int index;
        PosicionCombate repVisualObjetivo = null;
        if (objetivo is Character character && (index = aliados.lista.IndexOf(character)) >= 0)
        {
            repVisualObjetivo = representacionVisualAliados[index];
        }
        else if (objetivo is Monster monster && (index = enemigos.lista.IndexOf(monster)) >= 0)
        {
            repVisualObjetivo = representacionVisualEnemigos[index];
        }
        if (repVisualObjetivo != null)
            foreach (var element in elementos)
            {
                repVisualObjetivo.TriggerElement(element);
            }
    }

    /// <summary>Activa la animacion segun su ID sobre el personaje indicado</summary>
    /// <param name="personaje">La posicion de combate (representación visual) del personaje a animar</param>
    /// <param name="idTrigger">El ID de la animación</param>
    public void TriggerAnimation(PosicionCombate personaje, int idTrigger)
    {
        personaje.TriggerAnimation(idTrigger);
    }

    /// <summary>Devuelve una lista de personajes que defienden al objetivo indicado</summary>
    /// <typeparam name="T">El tipo al cual corresponde el personaje</typeparam>
    /// <param name="equipoObjetivo">La matriz representativa de las posiciones de los personajes del equipo</param>
    /// <param name="pos">la posición en retaguardia del personaje objetivo</param>
    /// <returns>La lista de defensores</returns>
    private List<T> GetDefensores<T>(T[][] equipoObjetivo, int pos) where T : Stats
    {
        List<T> defensores = new List<T>();
        T[] vanguardia = equipoObjetivo[0];

        //Bucle mientras i este entre las 5 posiciones delante del objetivo e i sea menor al límite del array
        /* 
        X = posicion retaguardia, Y = otros personajes de la formación, G = posiciones comprobadas
        Ejemplo 1:              Ejemplo 2:              Ejemplo 3:
        -Y-Y-       -YGGG       -YYY-       GGGGG       -Y-Y-       GGGG-
        Y-Y-X   =>  Y-Y-X       --X--   =>  --X--       -X-Y-   =>  -X-Y-
        Total defensores: 1     Total defensores: 3     Total defensores: 2

        Las formaciones mostradas son segun las posibles formaciones que puede dar la función PrepararFormacion
        */

        for (int i = pos - 2; i <= pos + 2 && i < vanguardia.Length; i++)
        {
            if (i < 0) continue;

            T temp = equipoObjetivo[0][i];

            if (temp != null && temp.hp > 0)
            {
                defensores.Add(temp);
            }
        }
        return defensores;
    }

    /// <summary>Devuelve los personajes que pueden ser atacados a partir de las características del ataque/objeto</summary>
    /// <param name="area">Área de efecto</param>
    /// <param name="target">Equipos seleccionables</param>
    /// <returns>Lista de posibles objetivos para el ataque/objeto</returns>
    public List<PosicionCombate> GetTargets(Area area, Target target) //TODO si añadimos hechizos de resurreccion hay que añadir aqui un booleano para saber si queremos muertos o vivos
    {
        List<PosicionCombate> personajesEnElPuntoDeMira = new List<PosicionCombate>();

        switch (target)
        {
            case Target.ENEMIES:
                personajesEnElPuntoDeMira.AddRange(representacionVisualEnemigos);
                break;
            case Target.ALLIES:
                personajesEnElPuntoDeMira.AddRange(representacionVisualAliados);
                break;
            case Target.ALL:
                personajesEnElPuntoDeMira.AddRange(representacionVisualGeneral);
                break;
            default:
                break;
        }

        return personajesEnElPuntoDeMira;
    }

    /// <summary>Obtiene la lista de nombres de los personajes pasados por parámetro</summary>
    /// <param name="personajesEnElPuntoDeMira">Los personajes de los que queremos los nombres</param>
    /// <returns>La lista de nombres</returns>
    private List<string> GetNames(List<Stats> personajesEnElPuntoDeMira)
    {
        List<string> listaNombres = new List<string>();
        foreach (var personaje in personajesEnElPuntoDeMira)
        {
            listaNombres.Add(personaje.nombre);
        }
        return listaNombres;
    }

    /// <summary>Obtiene la lista de nombres de los personajes pasados por parámetro</summary>
    /// <param name="personajesEnElPuntoDeMira">Los personajes de los que queremos los nombres</param>
    /// <returns>La lista de nombres</returns>
    private List<string> GetNames(List<PosicionCombate> personajesEnElPuntoDeMira)
    {
        List<string> listaNombres = new List<string>();
        foreach (var item in personajesEnElPuntoDeMira)
        {
            listaNombres.Add(item.personaje.nombre);
        }
        return listaNombres;
    }

    private IEnumerator Victoria()
    {
        eventoZonaCombate.Delay(null);
        foreach (var personaje in aliados.lista)
        {
            BooleanAnimation(personaje, animVictoriaBool, true);
        }
        yield return new WaitForSecondsRealtime(3f);
        SceneManager.LoadScene("Victory");
        yield return null;
    }

    private IEnumerator Derrota()
    {
        eventoZonaCombate.Delay(null);
        foreach (var personaje in enemigos.lista)
        {
            BooleanAnimation(personaje, animVictoriaBool, true);
        }
        yield return new WaitForSecondsRealtime(3f);
        SceneManager.LoadScene("GameOver");
        yield return null;
    }

    #endregion



}

public enum EstadoTurno
{
    EN_ESPERA = 0,
    MENU_PRINCIPAL = 1,
    MENU_SKILLS = 2,
    MENU_ITEMS = 3,
    MENU_FORMACION = 4,
    HUYENDO = 5,
    ATAQUE = 6,
    TURNO_TERMINADO = 10
}
