﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasOverworldController : MonoBehaviour
{
    public Sprite[] iconos;
    public Image i;
    public Image e;
    public Sprite[] exploration;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PonerEscudo()
    {
        i.sprite = iconos[0];
    }

    public void PonerEspadas()
    {
        i.sprite = iconos[1];
    }

}
