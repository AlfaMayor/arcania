﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CanvasDialogoController : MonoBehaviour
{
    public List<Sprite> SpritesTest;
    public DialogueData datosDialogos;
    public List<Dialogo> dialogosPrueba = new List<Dialogo>();
    public Image p;
    public Canvas c;
    public Text t;
    public Image i;

    private void Awake()
    {
        c = GetComponent<Canvas>(); //El canvas tiene que estar en el mismo objeto
    }
    // Start is called before the first frame update
    void Start()
    {
        c.enabled = false;
        if (datosDialogos.Dialogos.Count == 0) //Testeo, añade lineas por defecto si no hay guardadas
        {
            var dialogoTest = Dialogo.CreateInstance<Dialogo>();
            dialogoTest.LineList = new List<string>(new string[] { "Hola Edrik! Como te va la vida chico?", "Hola Tarkoy, sigues tan viejo como siempre" });
            dialogoTest.SpriteList = SpritesTest;
            // { LineList = { "Hola Edrik! Como te va la vida chico?", "Hola Tarkoy, sigues tan viejo como siempre" }, SpriteList = SpritestTest }
            datosDialogos.Dialogos.Add(dialogoTest);
        }
    }

    public bool GetLineaDialogo(int dialogoId, int dialogoLinea)
    {
        if (dialogoId < datosDialogos.Dialogos.Count)
        {
            if (!c.enabled) c.enabled = true; //Muestra la ventana de diálogo si no está activa

            //Recoge los datos del diálogo a mostrar y los asigna, en caso de intentar mostrar un diálogo no válido, muestra valores por defecto
            //TODO intentar que no se tengan que usar los valores por defecto, almenos en texto (en imagenes puede ser útil)

            Dialogo dialogo = datosDialogos.Dialogos[dialogoId];

            if (dialogoLinea >= dialogo.ColorList.Count) p.color = Color.white;
            else p.color = dialogo.ColorList[dialogoLinea];

            if (dialogoLinea >= dialogo.SpriteList.Count) i.sprite = null;
            else i.sprite = dialogo.SpriteList[dialogoLinea];

            if (dialogoLinea >= dialogo.LineList.Count) t.text = "...";
            else t.text = dialogo.LineList[dialogoLinea];

            bool flag = dialogoLinea >= datosDialogos.Dialogos[dialogoId].LineList.Count;

            if (flag) c.enabled = false; //Si ya ha terminado el diálogo cierra la ventana de diálogo

            return flag; //Devuelve true si ya hemos llegado al final del dialogo
        }
        return false;
    }
}
