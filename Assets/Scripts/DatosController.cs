﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatosController : MonoBehaviour
{

    private static DatosController singleton;

    public static DatosController Singleton
    {
        get
        {
            if (!singleton)
            {
                singleton = FindObjectOfType<DatosController>();
            }
            return singleton;
        }
    }

    void Awake()
    {
        if (singleton != null && singleton != this)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
            DontDestroyOnLoad(this);
        }
    }

    public SaveGameData currentSave;

    public bool overworldIniciado = false;

    public FormacionEquipo<Monster> equipoEnemigo;

    public int currentArea = 0;

    public Inventory inventario;

}
