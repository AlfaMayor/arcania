﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

/// <summary>Guarda la formación del equipo</summary>
/// <typeparam name="T"></typeparam>
[Serializable]
public class FormacionEquipo<T> where T : Stats
{
    /// <summary>Indica el equilibrio del grupo (la diferencia en numero entre vanguardia y retaguardia). 
    /// También indica el índice del primer personaje de retaguardia.
    /// Mismo valor que lista.Count si no hay retaguardia</summary>
    public int numEnVanguardia = 0;

    /// <summary>La lista de personajes. 
    /// El orden por línea es 42135 (de centro a extremos priorizando inicio de la línea)</summary>
    public List<T> lista = new List<T>();

    //Constructor clon
    public FormacionEquipo(FormacionEquipo<T> original, bool clonaEntidades = true)
    {
        foreach (var personaje in original.lista)
        {
            lista.Add(clonaEntidades ? ScriptableObject.Instantiate(personaje) : personaje);
        }
        numEnVanguardia = original.numEnVanguardia;
    }

    /// <summary>Constructor de equipo pasando el equipo entero. 
    /// Pone los 5 personajes indicados en el selector o los 5 primeros 
    /// (o todos si hay 5 o menos en la lista de personajes)</summary>
    /// <param name="lista">El grupo de personajes</param>
    /// <param name="numEnVanguardia">El número de personajes en la vanguardia del equipo 
    /// (también indica la posición del primer personaje en retaguardia)</param>
    /// <param name="selector">Los personajes que deben añadirse a la formación</param>
    public FormacionEquipo(List<T> lista, int numEnVanguardia = 0, List<int> selector = null)
    {
        if (selector == null || selector.Count == 0)
        {
            selector = new List<int>(new int[] { 0, 1, 2, 3, 4 });
        }

        foreach (var i in selector)
        {
            if (i >= 0 && i < lista.Count)
            {
                this.lista.Add(lista[i]);
                if (this.lista.Count >= 5) break;
            }
        }

        //Asegura que numEnVanguardia no supera la cantidad de personajes del grupo ni el límite por línea y tampoco es demasiado bajo
        this.numEnVanguardia = Mathf.Clamp(Mathf.Clamp(numEnVanguardia, lista.Count >= 5 ? 2 : 1, lista.Count), 1, 3);
    }
}

/// <summary>FormacionEquipo con unos añadidos para usarse en la escena de combate</summary>
/// <typeparam name="T">La formación sólo puede tener el tipo de personaje definido aquí</typeparam>
public class FormacionCombate<T> : FormacionEquipo<T> where T : Stats
{
    public List<PosicionCombate> representacionVisual;
    public T[][] formacionEnCombate;

    public FormacionCombate(FormacionEquipo<T> original, bool clonaEntidades = true) : base(original, clonaEntidades)
    {
    }

    public FormacionCombate(List<T> lista, List<int> selector, int numEnVanguardia) : base(lista, numEnVanguardia, selector)
    {
    }

    public int SumaHp()
    {
        int sumahp = 0;
        for (int i = 0; i < lista.Count; i++)
        {
            sumahp += lista[i].hp;
        }
        return sumahp;
    }
}

/// <summary>Clase <see cref="FormacionCombate{T}"/> para debug (al no ser genérica se puede mostrar en el editor de Unity sin necesidad de un inspector personalizado)</summary>
[Serializable]
public class FormacionAliada : FormacionCombate<Character>
{
    public FormacionAliada(FormacionEquipo<Character> original) : base(original)
    {

    }
}

/// <summary>Clase <see cref="FormacionCombate{T}"/> para debug (al no ser genérica se puede mostrar en el editor de Unity sin necesidad de un inspector personalizado)</summary>
[Serializable]
public class FormacionEnemiga : FormacionCombate<Monster>
{
    public FormacionEnemiga(FormacionEquipo<Monster> original) : base(original)
    {
    }
}
