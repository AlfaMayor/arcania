﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatBonus
{
    public StatBonus()
    {

    }

    public StatBonus(StatBonus original)
    {
        hp = original.hp;
        mp = original.mp;
        ep = original.ep;
        atk = original.atk;
        def = original.def;
        mAt = original.mAt;
        mDf = original.mDf;
        agl = original.agl;
        eva = original.eva;
        acc = original.acc;
        lck = original.lck;
    }

    public int hp = 0, mp = 0, ep = 0, atk = 0, def = 0, mAt = 0, mDf = 0, agl = 0, eva = 0, acc = 0, lck = 0;
}
