﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
[CreateAssetMenu(fileName = "Pincel de Triggers", menuName = "Brushes/Pincel de Triggers")]
[CustomGridBrush(false, true, false, "Trigger brush")]
public class BrushTriggers : GridBrushBase
{

    private TriggerList triggerList;

    internal int triggerId = 0;
    public BaseTrigger triggerSeleccionado;
    public List<BaseTrigger> triggers = new List<BaseTrigger>();

    public bool sustituye = true;

    public float offsetX = 0f;
    public float offsetY = 0f;
    public int posZ = 0;


    public override void Paint(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
    {
        if (triggerList == null) triggerList = TriggerList.Singleton;

        triggerSeleccionado = triggers[triggerId];
        Vector3Int cellPosition = new Vector3Int(position.x, position.y, posZ);
        Vector3 centerTile = new Vector3(0.5f + offsetX, 0.5f + offsetY, 0f);

        BaseTrigger existingTrigger;
        //Si ya hay un Trigger en esa celda...
        if ((existingTrigger = GetObjectInCell(gridLayout, brushTarget.transform, cellPosition)) != null)
        {
            if (existingTrigger.GetType().Equals(triggerSeleccionado.GetType())) 
                return; //... Y es del mismo tipo no hace nada
            else if (sustituye) // ... Y el tipo no coincide y esta activada la sustitución de triggers
            {
                //Destruye el gameObject del trigger existente para poder poner uno distinto
                DestroyImmediate(existingTrigger.gameObject);
            }
        }

        GameObject go = Instantiate(triggerSeleccionado.gameObject);
        go.transform.SetParent(brushTarget.transform);
        go.transform.position = gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(cellPosition + centerTile));

        var tr = go.GetComponent<BaseTrigger>();

        triggerList.SaveTrigger(tr);

    }

    private static BaseTrigger GetObjectInCell(GridLayout grid, Transform parent, Vector3Int position)
    {
        //Obtengo la cantidad de hijos que tiene el padre
        int childCount = parent.childCount;

        //Cojo la posición en el mundo de la celda en la que se ha hecho clic...
        Vector3 min = grid.LocalToWorld(grid.CellToLocalInterpolated(position));

        //... Y añade (1f, 1f, 1f) para comprobar la celda entera
        Vector3 max = grid.LocalToWorld(grid.CellToLocalInterpolated(position + Vector3.one));

        Bounds bounds = new Bounds((max + min) * 0.5f, max - min);

        for (int i = 0; i < childCount; i++)
        {
            Transform child = parent.GetChild(i);
            BaseTrigger trigger = child.GetComponent<BaseTrigger>(); //Coge el trigger del objeto si lo tiene

            //Comprueba si ya hay un trigger dentro de la celda
            if (bounds.Contains(child.position) && trigger)
            {
                //Si lo hay lo devuelve
                return trigger;
            }
        }
        //Si no hay nada devuelvo null
        return null;

    }

}

[CustomEditor(typeof(BrushTriggers))]
public class BrushTriggersEditor : GridBrushEditorBase
{
    public override GameObject[] validTargets
    {
        get
        {
            return GameObject.FindObjectsOfType<Tilemap>().Select(x => x.gameObject).ToArray();
        }
    }

    private BrushTriggers bt { get { return target as BrushTriggers; } }

    public override void OnPaintSceneGUI(GridLayout gridLayout, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing)
    {
        var evt = Event.current;
        if (evt.type == EventType.KeyDown && evt.keyCode == KeyCode.Space)
        {
            //Selecciona el siguiente trigger al pulsar la tecla espacio
            bt.triggerId++;
            if (bt.triggerId >= bt.triggers.Count) bt.triggerId = 0;

            //Muestra que trigger estamos intentando pintar sin tener que mirar la lista
            if (bt.triggers[bt.triggerId]) bt.triggerSeleccionado = bt.triggers[bt.triggerId];
        }
        base.OnPaintSceneGUI(gridLayout, brushTarget, position, tool, executing);
    }

    public override void OnPaintInspectorGUI()
    {
        GUILayout.Space(5f);
        GUILayout.Label("Pulsa espacio para cambiar al siguiente trigger en la lista.");
        base.OnPaintInspectorGUI();
    }

}
#endif